#include "pch.h"
#include "InputManagerSFML.h"

#include <NoEngine/Core.h>
#include <SFMLImage/Window/WindowSFML.h>

#include <SFML/Graphics.hpp>

void no::InputManagerSFML::Initialize(WindowSFML* window)
{
	m_Window = window;
}

void no::InputManagerSFML::ProcessInput()
{
	auto& window = *m_Window->GetWindow();
	sf::Event e;
	while (window.pollEvent(e))
	{
		if (e.type == sf::Event::Closed)
		{
			Core::Quit();
		}

		for (auto& eventCallback : m_EventCallbacks)
		{
			eventCallback.second(e);
		}
	}
}

int no::InputManagerSFML::AddEventCallback(const std::function<void(const sf::Event & event)>& eventCallback)
{
	// URGENT replace with random
	auto id = int(time(0));
	m_EventCallbacks[id] = eventCallback;
	return id;
}

void no::InputManagerSFML::RemoveEventCallback(int id)
{
	m_EventCallbacks.erase(id);
}
