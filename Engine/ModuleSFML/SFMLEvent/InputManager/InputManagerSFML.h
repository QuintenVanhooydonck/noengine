#pragma once
#include <functional>
#include <map>

namespace sf
{
	class Event;
}

namespace no
{
	class WindowSFML;

	class InputManagerSFML
	{
	public:
		void Initialize(WindowSFML* window);
		void ProcessInput();

		int AddEventCallback(const std::function<void(const sf::Event & event)>& eventCallback);
		void RemoveEventCallback(int id);

	private:
		WindowSFML* m_Window;
		std::map<int, std::function<void(const sf::Event & event)>> m_EventCallbacks{};
	};
}