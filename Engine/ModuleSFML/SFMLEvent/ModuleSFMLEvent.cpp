#include "pch.h"
#include "ModuleSFMLEvent.h"

#include <assert.h>

#include <NoEngine/Render/Window.h>
#include <SFMLImage/Window/WindowSFML.h>
#include "NoEngine/Debug/Logger.h"

#include "InputManager/InputManagerSFML.h"

no::ModuleSFMLEvent::ModuleSFMLEvent()
{

}

no::ModuleSFMLEvent::~ModuleSFMLEvent()
{
	if (m_InputManager)
	{
		delete m_InputManager;
		m_InputManager = nullptr;
	}
}

void no::ModuleSFMLEvent::Update()
{
	if (m_InputManager)
		m_InputManager->ProcessInput();
}

void no::ModuleSFMLEvent::Initialize()
{
	m_InputManager = new InputManagerSFML();
	auto window = dynamic_cast<WindowSFML*>(Window::GetService());
	if (!window)
	{
		no::Logger::LOG_ERROR("Error couldn't load correct window!");
		assert(0);
	}
	m_InputManager->Initialize(window);
}
