#pragma once
#include "NoEngine/Modules/Module.h"

namespace no
{
	class InputManagerSFML;

	class ModuleSFMLEvent : public Module
	{
	public:
		ModuleSFMLEvent();
		~ModuleSFMLEvent();

		virtual void Update() override;
		virtual void Initialize() override;

		InputManagerSFML* GetInputManager() const { return m_InputManager; }

	private:
		InputManagerSFML* m_InputManager{};
	};
}