#pragma once
#include <NoEngine/Render/RenderTarget.h>
#include <SFML/Graphics/RenderTexture.hpp>

namespace sf { class RenderTexture; }

namespace no
{
	class RenderTargetSFML final : public RenderTarget
	{
	public:
		static void Setup();
		RenderTargetSFML(const glm::vec2& size);
		~RenderTargetSFML();

		RenderTargetSFML(RenderTargetSFML&& other) noexcept;
		RenderTargetSFML& operator=(RenderTargetSFML&& other) noexcept;

		virtual void SetTextureSize(const glm::vec2& size) override;

		sf::RenderTexture* getTexture() const { return m_RenderTexture; }

		virtual void* GetImagePointer(bool* flipHorizontal, bool* flipVertical) const override;

		virtual void OnRemoved() override;

	private:
		sf::RenderTexture* m_RenderTexture{};

		static RenderTarget* Create(const glm::vec2& size);



	};
}