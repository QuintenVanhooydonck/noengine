#include "pch.h"
#include "TextureLoaderSFML.h"

//Engine Files
#include "NoEngine/Resources/Internal/Descriptors/TextureDescriptor.h"
#include "NoEngine/Resources/Internal/ResourceData.h"

// SFML Files
#include <SFML/Graphics.hpp>

// Module Files
#include "TextureSFML.h"
#include "NoEngine/Debug/Logger.h"

no::TextureLoaderSFML::TextureLoaderSFML()
{
}

no::ITexture* no::TextureLoaderSFML::GetResource(IDescriptor* desc)
{
	const auto fullPath = ResourceData::baseFilePath + static_cast<TextureDescriptor*>(desc)->m_Path.GetString();
	sf::Texture* texture = new sf::Texture();
	if (!texture->loadFromFile(fullPath))
	{
		no::Logger::LOG_WARNING("Couldn't load file with path " + fullPath);
	}

	return new TextureSFML(texture);
}
