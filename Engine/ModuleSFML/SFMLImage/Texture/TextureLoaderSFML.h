#pragma once
#include "NoEngine/Resources/Internal/IResourceLoader.h"

namespace no
{
	class ITexture;
	struct IDescriptor;

	class TextureLoaderSFML : public IResourceLoader<ITexture>
	{
	public:
		TextureLoaderSFML();

		ITexture* GetResource(IDescriptor* desc);
	};
}