#pragma once
#include "NoEngine/Resources/Internal/ITexture.h"

namespace sf { 
	class Texture; 
	class Sprite;
}

namespace no
{
	class RendererSFML;

	class TextureSFML : public ITexture
	{
	public:
		TextureSFML(sf::Texture* pTexture);
		~TextureSFML();

		virtual void Render(float x, float y) override;

		virtual void RenderImmediate(float x, float y) override;

		virtual void SetColor(const glm::vec4& color) override;

	private:
		sf::Texture* m_pTexture;
		sf::Sprite* m_pSprite;
		RendererSFML& m_Renderer;
		glm::uvec4 m_Color{ 1, 1, 1, 1 };

		void BeforeRender();
	};
}