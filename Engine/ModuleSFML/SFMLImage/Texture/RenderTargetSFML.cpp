#include "pch.h"
#include "RenderTargetSFML.h"
#include "SFML/Graphics/RenderTarget.hpp"

void no::RenderTargetSFML::Setup()
{
	SetFactoryFunction(&no::RenderTargetSFML::Create);
}

no::RenderTargetSFML::~RenderTargetSFML()
{
	if (m_RenderTexture)
	{
		delete m_RenderTexture;
		m_RenderTexture = nullptr;
	}
}

no::RenderTargetSFML::RenderTargetSFML(RenderTargetSFML&& other) noexcept
	:RenderTarget(std::move(other)),
	m_RenderTexture{other.m_RenderTexture}
{
	other.m_RenderTexture = nullptr;
}

no::RenderTargetSFML& no::RenderTargetSFML::operator=(RenderTargetSFML&& other) noexcept
{
	auto preventWarning = RenderTarget(std::move(other));
	(preventWarning);

	if (m_RenderTexture)
	{
		delete m_RenderTexture;
		m_RenderTexture = nullptr;
	}

	m_RenderTexture = other.m_RenderTexture;

	other.m_RenderTexture = nullptr;

	return *this;
}

no::RenderTargetSFML::RenderTargetSFML(const glm::vec2& size)
	:RenderTarget(size),
	m_RenderTexture{new sf::RenderTexture()}
{
	m_RenderTexture->create(500, 500);
}

void no::RenderTargetSFML::SetTextureSize(const glm::vec2& size)
{
	if (m_Size == size)
		return;

	RenderTarget::SetTextureSize(size);

	if (m_RenderTexture)
	{
		delete m_RenderTexture;
		m_RenderTexture = nullptr;
	}

	m_RenderTexture = new sf::RenderTexture();
	m_RenderTexture->create(int(m_Size.x + 0.5f), int(m_Size.y + 0.5f));
}

no::RenderTarget* no::RenderTargetSFML::Create(const glm::vec2& size)
{
	return new RenderTargetSFML(size);
}

void* no::RenderTargetSFML::GetImagePointer(bool* flipHorizontal = nullptr, bool* flipVertical = nullptr) const
{
	*flipHorizontal = false;
	*flipVertical = true;
	return (void*)(size_t)m_RenderTexture->getTexture().getNativeHandle();
}

void no::RenderTargetSFML::OnRemoved()
{
	m_RenderTexture->display();
}
