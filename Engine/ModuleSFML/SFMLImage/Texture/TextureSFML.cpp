#include "pch.h"
#include "TextureSFML.h"

#include "NoEngine/Render/Renderer.h"

#include <SFML/Graphics.hpp>

#include "../Renderer/RendererSFML.h"

no::TextureSFML::TextureSFML(sf::Texture* pTexture)
	:m_pTexture{ pTexture },
	m_pSprite{new sf::Sprite(*pTexture)},
	m_Renderer{ *static_cast<RendererSFML*>(Renderer::GetService()) }
{

}

no::TextureSFML::~TextureSFML()
{
	if (m_pSprite)
	{
		delete m_pSprite;
		m_pSprite = nullptr;
	}

	if (m_pTexture)
	{
		delete m_pTexture;
		m_pTexture = nullptr;
	}
}

void no::TextureSFML::Render(float x, float y)
{
	BeforeRender();
	m_pSprite->setPosition(x, y);
	m_Renderer.RenderTexture(m_pSprite);
}

void no::TextureSFML::RenderImmediate(float x, float y)
{
	BeforeRender();
	m_pSprite->setPosition(x, y);
	m_Renderer.RenderTextureImmediate(m_pSprite);
}

void no::TextureSFML::SetColor(const glm::vec4& color)
{
	m_Color = color;
}

void no::TextureSFML::BeforeRender()
{
	sf::Color color;
	color.r = sf::Uint8(m_Color.r * 255);
	color.g = sf::Uint8(m_Color.g * 255);
	color.b = sf::Uint8(m_Color.b * 255);
	color.a = sf::Uint8(m_Color.a * 255);
	m_pSprite->setColor(color);
}
