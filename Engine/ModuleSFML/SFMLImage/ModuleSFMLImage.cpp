#include "pch.h"
#include "ModuleSFMLImage.h"

#include <NoEngine/Render/Window.h>
#include <NoEngine/Core.h>
#include <NoEngine/Render/Renderer.h>
#include <NoEngine/resources/Internal/ITexture.h>
#include <NoEngine/Resources/Internal/IResourceManager.h>
#include <NoEngine/Resources/Internal/ResourceManagerChached.h>
#include <NoEngine/Resources/Internal/ResourceManagers/TextureManager.h>
#include <NoEngine/Resources/Internal/ResourceManagers/FontManager.h>
#include <SFMLEvent/ModuleSFMLEvent.h>

#include <SFML/Graphics/RenderWindow.hpp>

#include "Window/WindowSFML.h"
#include "Renderer/RendererSFML.h"
#include "Texture/TextureLoaderSFML.h"
#include "Font/FontLoaderSFML.h"
#include "Texture/RenderTargetSFML.h"

no::ModuleSFMLImage::ModuleSFMLImage(int screenWidth, int screenHeight, const std::string& windowName)
{
	InitResourceManager();
	InitRenderer(screenWidth, screenHeight, windowName);
}

no::ModuleSFMLImage::~ModuleSFMLImage()
{
	TextureManager::Destroy();
	FontManager::Destroy();

	Renderer::Destroy();
	Window::Destroy();
}

void no::ModuleSFMLImage::InitResourceManager()
{
	TextureLoaderSFML* textureLoader = new TextureLoaderSFML();
	IResourceManager<ITexture>* textureManager = new ResourceManagerCached<ITexture>(textureLoader);
	TextureManager::Provide(textureManager);

	FontLoaderSFML* fontLoader = new FontLoaderSFML();
	IResourceManager<IFont>* fontManager = new ResourceManagerCached<IFont>(fontLoader);
	FontManager::Provide(fontManager);
}

void no::ModuleSFMLImage::InitRenderer(int screenWidth, int screenHeight, const std::string& windowName)
{
	m_Window = new WindowSFML(screenWidth, screenHeight, windowName);
	Window::Provide(m_Window);

	RendererSFML* renderer = new RendererSFML();
	renderer->Init(m_Window);
	Renderer::Provide(renderer);

	RenderTargetSFML::Setup();
}

void no::ModuleSFMLImage::Initialize()
{
	//NoEngine::RequireModule<ModuleSFMLEvent>();
}
