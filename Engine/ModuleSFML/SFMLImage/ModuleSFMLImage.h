#pragma once
#include "NoEngine/Modules/Module.h"

namespace no
{
	class WindowSFML;

	class ModuleSFMLImage final : public Module
	{
	public:
		ModuleSFMLImage(int screenWidth, int screenHeight, const std::string& windowName);
		~ModuleSFMLImage();

	private:
		WindowSFML* m_Window{};

		void InitResourceManager();
		void InitRenderer(int screenWidth, int screenHeight, const std::string& windowName);

		virtual void Initialize() override;

	};
}