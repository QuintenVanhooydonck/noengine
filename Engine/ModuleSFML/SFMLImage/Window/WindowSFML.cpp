#include "pch.h"
#include "WindowSFML.h"

#include <SFML/Graphics.hpp>

no::WindowSFML::WindowSFML(int width, int height, const std::string& name)
	:m_Width{ width },
	m_Height{ height },
	m_ScreenMode{ ScreenMode::windowed },
	m_WindowName{ name },
	m_EnableVSync{ true },
	m_Resizeable{ true },
	m_Window{}
{
	CreateWindow();
}

no::WindowSFML::~WindowSFML()
{
	if (m_Window)
	{
		m_Window->close();
		delete m_Window;
		m_Window = nullptr;
	}
}

void no::WindowSFML::SetWindowFullscreen(ScreenMode screenMode)
{
	m_ScreenMode = screenMode;
	CreateWindow();
}

void no::WindowSFML::SetWindowSize(int width, int height)
{
	m_Width = width;
	m_Height = height;
	m_Window->setSize({ unsigned int(width), unsigned int(height) });
}

void no::WindowSFML::SetWindowResizable(bool resizeable)
{
	if (m_Resizeable == resizeable)
		return;

	m_Resizeable = resizeable;
	CreateWindow();
}

void no::WindowSFML::SetWindowTitle(const std::string& name)
{
	m_WindowName = name;
	m_Window->setTitle(name);
}

void no::WindowSFML::SetVsync(bool on)
{
	m_EnableVSync = on;
	m_Window->setVerticalSyncEnabled(on);
}

void no::WindowSFML::Close()
{
	m_Window->close();
}

void no::WindowSFML::CreateWindow()
{
	if (m_Window)
	{
		delete m_Window;
		m_Window = nullptr;
	}

	sf::Uint32 style = 0;
	switch (m_ScreenMode)
	{
	case no::ScreenMode::fullscreen:
		style |= sf::Style::Fullscreen;
		break;
	case no::ScreenMode::borderlessFullscreen:
		break;
	case no::ScreenMode::windowed:
		style |= sf::Style::Titlebar;
		style |= sf::Style::Close;
		break;
	}

	if (m_Resizeable)
		style |= sf::Style::Resize;

	m_Window = new sf::RenderWindow(sf::VideoMode(m_Width, m_Height), m_WindowName, style);
	m_Window->setVerticalSyncEnabled(m_EnableVSync);
	m_Window->display();
}

glm::vec2 no::WindowSFML::GetWindowSize() const
{
	auto size = m_Window->getSize();
	return { float(size.x), float(size.y) };
}
