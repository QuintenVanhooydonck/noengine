#pragma once
#include "NoEngine/Render/Internal/IWindow.h"

namespace sf { class RenderWindow; }

namespace no
{
	class WindowSFML final : public IWindow
	{
	public:
		WindowSFML(int width, int height, const std::string& name);
		~WindowSFML();

		virtual void SetWindowFullscreen(ScreenMode screenMode) override;
		virtual void SetWindowSize(int width, int height) override;
		virtual void SetWindowResizable(bool resizeable) override;
		virtual void SetWindowTitle(const std::string& name) override;
		virtual void SetVsync(bool on) override;
		virtual void Close() override;

		sf::RenderWindow* GetWindow() const { return m_Window; }

		virtual glm::vec2 GetWindowSize() const override;

	private:
		sf::RenderWindow* m_Window;
		std::string m_WindowName;
		ScreenMode m_ScreenMode;
		int m_Width, m_Height;
		bool m_EnableVSync, m_Resizeable;

		void CreateWindow();


	};
}