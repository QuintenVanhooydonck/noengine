#pragma once
#include "NoEngine/Resources/Internal/IFontTexture.h"
#include "NoEngine/Math/Vectors.h"

namespace sf
{
	class Text;
}

namespace no
{
	class RendererSFML;

	class FontTextureSFML : public IFontTexture
	{
	public:
		FontTextureSFML(sf::Text* pTexture);
		~FontTextureSFML();

		virtual void Render(float x, float y) override;
		virtual void RenderImmediate(float x, float y) override;

		virtual void SetColor(const glm::vec4& color);

	private:
		sf::Text* m_pTexture;
		RendererSFML& m_Renderer;
		glm::vec4 m_Color{ 1, 1, 1, 1 };

		void BeforeRender();
	};
}