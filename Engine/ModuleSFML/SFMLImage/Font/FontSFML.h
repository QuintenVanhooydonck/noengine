#pragma once
#include "NoEngine/Resources/Internal/IFont.h"

namespace sf { class Font; }

namespace no
{
	class RendererSFML;

	class FontSFML final : public IFont
	{
	public:
		FontSFML(sf::Font* pFont);
		~FontSFML();

		virtual IFontTexture* GetFontTexture(const std::string& text, size_t fontSize) override;

	private:
		sf::Font* m_pFont{};
		RendererSFML& m_Renderer;
	};
}