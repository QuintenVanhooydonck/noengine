#include "pch.h"
#include "FontSFML.h"

#include "NoEngine/Render/Renderer.h"

#include <SFML/Graphics.hpp>

#include "../Renderer/RendererSFML.h"
#include "FontTextureSFML.h"

no::FontSFML::FontSFML(sf::Font* pFont)
	:m_pFont{ pFont },
	m_Renderer{ *static_cast<RendererSFML*>(Renderer::GetService()) }
{
}

no::FontSFML::~FontSFML()
{
	if (m_pFont)
	{
		delete m_pFont;
		m_pFont = nullptr;
	}
}

no::IFontTexture* no::FontSFML::GetFontTexture(const std::string& text, size_t fontSize)
{
	auto fontTexture = new sf::Text(text, *m_pFont, unsigned int(fontSize));
	return new FontTextureSFML(fontTexture);
}
