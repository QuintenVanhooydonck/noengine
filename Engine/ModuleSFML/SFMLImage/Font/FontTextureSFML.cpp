#include "pch.h"
#include "FontTextureSFML.h"

#include <NoEngine/Render/Renderer.h>

#include <SFML/Graphics.hpp>

#include "../Renderer/RendererSFML.h"

no::FontTextureSFML::FontTextureSFML(sf::Text* pTexture)
	:m_pTexture{ pTexture },
	m_Renderer{ *static_cast<RendererSFML*>(Renderer::GetService()) }
{
}

no::FontTextureSFML::~FontTextureSFML()
{
	if (m_pTexture)
	{
		delete m_pTexture;
		m_pTexture = nullptr;
	}
}

void no::FontTextureSFML::Render(float x, float y)
{
	BeforeRender();
	m_pTexture->setPosition(x, y);
	m_Renderer.RenderTexture(m_pTexture);
}

void no::FontTextureSFML::RenderImmediate(float x, float y)
{
	BeforeRender();
	m_pTexture->setPosition(x, y);
	m_Renderer.RenderTextureImmediate(m_pTexture);
}

void no::FontTextureSFML::SetColor(const glm::vec4& color)
{
	m_Color = color;
}

void no::FontTextureSFML::BeforeRender()
{
	sf::Color color;
	color.r = sf::Uint8(m_Color.r * 255);
	color.g = sf::Uint8(m_Color.g * 255);
	color.b = sf::Uint8(m_Color.b * 255);
	color.a = sf::Uint8(m_Color.a * 255);
	m_pTexture->setFillColor(color);
}
