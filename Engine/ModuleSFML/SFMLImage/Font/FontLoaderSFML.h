#pragma once
#include "NoEngine/Resources/Internal/IResourceLoader.h"

namespace no
{
	class IFont;
	struct IDescriptor;

	class FontLoaderSFML : public IResourceLoader<IFont>
	{
	public: 
		FontLoaderSFML();

		IFont* GetResource(IDescriptor* desc);
	};

}