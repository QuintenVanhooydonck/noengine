#include "pch.h"
#include "FontLoaderSFML.h"

// Engine Files
#include "NoEngine/Resources/Internal/Descriptors/FontDescriptor.h"
#include "NoEngine/Resources/Internal/IFont.h"
#include "NoEngine/Resources/Internal/ResourceData.h"
#include "NoEngine/Debug/Logger.h"

#include <SFML/Graphics.hpp>

#include "FontSFML.h"

no::FontLoaderSFML::FontLoaderSFML()
{
}

no::IFont* no::FontLoaderSFML::GetResource(IDescriptor* desc)
{
	const auto fullPath = ResourceData::baseFilePath + static_cast<FontDescriptor*>(desc)->m_Path.GetString();
	sf::Font* pFont = new sf::Font();
	if (!pFont->loadFromFile(fullPath))
	{
		no::Logger::LOG_WARNING("Couldn't load file with path " + fullPath);
	}

	return new FontSFML(pFont);
}
