#include "pch.h"
#include "RendererSFML.h"

#include "NoEngine/Scene/SceneManager.h"

#include <SFML/Graphics.hpp>

#include "../Window/WindowSFML.h"
#include <NoEngine/Render/RenderTarget.h>
#include "SFMLImage/Texture/RenderTargetSFML.h"

void no::RendererSFML::Init(WindowSFML* window)
{
	m_pWindow = window;
	m_DrawSurface = m_pWindow->GetWindow();
}

void no::RendererSFML::Render() const
{
	SceneManager::GetInstance().Render();
}

void no::RendererSFML::StartRender() const
{
	m_pWindow->GetWindow()->clear();
}

void no::RendererSFML::EndRender() const
{
	m_pWindow->GetWindow()->display();
}

void no::RendererSFML::RenderTexture(sf::Drawable* texture) const
{
	m_DrawSurface->draw(*texture);
}

void no::RendererSFML::RenderTextureImmediate(sf::Drawable* texture) const
{
	m_pWindow->GetWindow()->draw(*texture);
	m_pWindow->GetWindow()->display();
}

void no::RendererSFML::Clear() const
{
	m_pWindow->GetWindow()->clear();
}

void no::RendererSFML::SetRenderTarget(RenderTarget* renderTarget)
{
	if (!renderTarget)
		return;

	if (m_RenderTarget)
	{
		m_RenderTarget->OnRemoved();
		m_RenderTarget = nullptr;
	}

	m_RenderTarget = static_cast<RenderTargetSFML*>(renderTarget);
	m_DrawSurface = m_RenderTarget->getTexture();
	m_RenderTarget->OnSet();
}

void no::RendererSFML::resetRenderTarget()
{
	if (m_RenderTarget)
	{
		m_RenderTarget->OnRemoved();
		m_RenderTarget = nullptr;
	}

	m_DrawSurface = m_pWindow->GetWindow();
}
