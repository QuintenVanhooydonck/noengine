#pragma once
#include "NoEngine/Render/Internal/IRenderer.h"

namespace sf {
	class Drawable;
	class RenderTarget;
}

namespace no
{
	class WindowSFML;
	class RenderTarget;
	class RenderTargetSFML;

	class RendererSFML final : public IRenderer
	{
	public:
		void Init(WindowSFML* window);
		virtual void Render() const override;

		void RenderTexture(sf::Drawable* texture) const;
		void RenderTextureImmediate(sf::Drawable* texture) const;
		virtual void StartRender() const override;
		virtual void EndRender() const override;

		virtual void Clear() const override;

		virtual void SetRenderTarget(RenderTarget* renderTarget) override;

		virtual void resetRenderTarget() override;

	private:
		WindowSFML* m_pWindow{};
		no::RenderTargetSFML* m_RenderTarget{};
		sf::RenderTarget* m_DrawSurface{};
	};
}