#pragma once
#include "NoEngine/Modules/Module.h"

namespace no
{
	class InputManagerSDL;

	class ModuleSDLEvent : public Module
	{
	public:
		ModuleSDLEvent();
		~ModuleSDLEvent();

		virtual void Update() override;

	private:
		InputManagerSDL* m_InputManager{};
	};
}