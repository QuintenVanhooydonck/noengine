#include "pch.h"
#include "InputManagerSDL.h"

#include "NoEngine/Core.h"

#include <SDL.h>

void no::InputManagerSDL::ProcessInput()
{
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) 
		{
			Core::Quit();
		}
		if (e.type == SDL_WINDOWEVENT)
		{
			if (e.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				Core::Quit();
			}
		}
		if (e.type == SDL_KEYDOWN) 
		{

		}
		if (e.type == SDL_MOUSEBUTTONDOWN) 
		{
		}
	}
}
