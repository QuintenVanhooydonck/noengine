#include "pch.h"
#include "ModuleSDLEvent.h"
#include "InputManager/InputManagerSDL.h"

no::ModuleSDLEvent::ModuleSDLEvent()
{
	m_InputManager = new InputManagerSDL();
}

no::ModuleSDLEvent::~ModuleSDLEvent()
{
	if (m_InputManager)
	{
		delete m_InputManager;
		m_InputManager = nullptr;
	}
}

void no::ModuleSDLEvent::Update()
{
	m_InputManager->ProcessInput();
}
