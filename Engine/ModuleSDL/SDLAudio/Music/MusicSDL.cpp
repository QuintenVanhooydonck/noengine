#include "pch.h"
#include "MusicSDL.h"

#include <SDL_mixer.h>

no::MusicSDL::MusicSDL(Mix_Music* music)
	:m_pMusic{ music }
{
}

no::MusicSDL::~MusicSDL()
{
	if (m_pMusic)
	{
		Mix_FreeMusic(m_pMusic);
		m_pMusic = nullptr;
	}
}

void no::MusicSDL::Play()
{
	if (m_pMusic)
		Mix_PlayMusic(m_pMusic, -1);
}

void no::MusicSDL::Pause()
{
	if (m_pMusic)
		Mix_PauseMusic();
}

void no::MusicSDL::Resume()
{
	if (m_pMusic)
		Mix_ResumeMusic();
}

void no::MusicSDL::Stop()
{
	if (m_pMusic)
		Mix_HaltMusic();
}
