#pragma once
#include "NoEngine/Resources/Internal/IMusic.h"

typedef struct _Mix_Music Mix_Music;

namespace no
{
	class MusicSDL : public IMusic
	{
	public:
		MusicSDL(Mix_Music* music);
		~MusicSDL();

		virtual void Play() override;
		virtual void Pause() override;
		virtual void Resume() override;
		virtual void Stop() override;

	private:
		Mix_Music* m_pMusic;
	};
}
