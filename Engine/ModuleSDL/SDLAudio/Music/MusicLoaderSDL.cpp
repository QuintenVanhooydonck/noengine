#include "pch.h"
#include "MusicLoaderSDL.h"

// Engine Files
#include "NoEngine/Resources/Internal/ResourceData.h"
#include "NoEngine/Resources/Internal/Descriptors/MusicDescriptor.h"
#include "NoEngine/Debug/Logger.h"

// SDL Files
#include <SDL_mixer.h>

// Module Files
#include "MusicSDL.h"

no::MusicLoaderSDL::MusicLoaderSDL()
{
	if ((Mix_Init(MIX_INIT_MP3) & MIX_INIT_MP3) != MIX_INIT_MP3)
	{
		throw std::runtime_error(std::string("Failed to load support for mp3s: ") + SDL_GetError());
	}
	if ((Mix_Init(MIX_INIT_OGG) & MIX_INIT_OGG) != MIX_INIT_OGG)
	{
		throw std::runtime_error(std::string("Failed to load support for oggs: ") + SDL_GetError());
	}
}

no::IMusic* no::MusicLoaderSDL::GetResource(IDescriptor* desc)
{
	const auto fullPath = ResourceData::baseFilePath + static_cast<MusicDescriptor*>(desc)->m_Path.GetString();
	auto pMusic = Mix_LoadMUS(fullPath.c_str());
	if (pMusic == nullptr)
	{
		no::Logger::LOG_WARNING("Couldn't load file with path " + fullPath + " \"" + SDL_GetError() + '\"');
	}

	return new MusicSDL(pMusic);
}
