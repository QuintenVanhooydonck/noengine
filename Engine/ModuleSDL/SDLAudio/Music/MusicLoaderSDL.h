#pragma once
#include "NoEngine/Resources/Internal/IResourceLoader.h"

namespace no
{
	class IMusic;
	struct IDescriptor;

	class MusicLoaderSDL : public IResourceLoader<IMusic>
	{
	public:
		MusicLoaderSDL();

		virtual IMusic* GetResource(IDescriptor* desc) override;
	};
}