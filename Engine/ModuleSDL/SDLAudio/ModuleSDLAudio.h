#pragma once
#include "NoEngine/Modules/Module.h"

namespace no
{
	class ModuleSDLAudio final : public Module
	{
	public:
		ModuleSDLAudio();
		~ModuleSDLAudio();

	private:
		void InitResourceManager();
		void InitSDL();
	};
}