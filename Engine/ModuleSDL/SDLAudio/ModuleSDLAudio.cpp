#include "pch.h"
#include "ModuleSDLAudio.h"

// Engine Files
#include "NoEngine/Resources/Internal/IResourceManager.h"
#include "NoEngine/Resources/Internal/ISound.h"
#include "NoEngine/Resources/Internal/ResourceManagers/SoundManager.h"
#include "NoEngine/Resources/Internal/ResourceManagers/MusicManager.h"
#include "NoEngine/Resources/Internal/ResourceManagerChached.h"

// SDL Files
#include <SDL.h>
#include <SDL_mixer.h>

// Module Files
#include "Sound/SoundLoaderSDL.h"
#include "Music/MusicLoaderSDL.h"

no::ModuleSDLAudio::ModuleSDLAudio()
{
	InitSDL();
	InitResourceManager();
}

no::ModuleSDLAudio::~ModuleSDLAudio()
{
	SoundManager::Destroy();
	MusicManager::Destroy();
}

void no::ModuleSDLAudio::InitResourceManager()
{
	IResourceLoader<ISound>* soundLoader = new SoundLoaderSDL();
	IResourceManager<ISound>* soundManager = new ResourceManagerCached<ISound>(soundLoader);
	SoundManager::Provide(soundManager);

	IResourceLoader<IMusic>* musicLoader = new MusicLoaderSDL();
	IResourceManager<IMusic>* musicManager = new ResourceManagerCached<IMusic>(musicLoader);
	MusicManager::Provide(musicManager);
}

void no::ModuleSDLAudio::InitSDL()
{
	if (SDL_Init(SDL_INIT_AUDIO) != 0)
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) != 0)
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}
}
