#include "pch.h"
#include "SoundLoaderSDL.h"

// Engine Files
#include "NoEngine/Resources/Internal/ResourceData.h"
#include "NoEngine/Resources/Internal/Descriptors/SoundDescriptor.h"
#include "NoEngine/Debug/Logger.h"

// SDL Files
#include <SDL_mixer.h>

// Module Files
#include "SoundSDL.h"

no::SoundLoaderSDL::SoundLoaderSDL()
{
	// no init required
}

no::ISound* no::SoundLoaderSDL::GetResource(IDescriptor* desc)
{
	const auto fullPath = ResourceData::baseFilePath + static_cast<SoundDescriptor*>(desc)->m_Path.GetString();
	auto sound = Mix_LoadWAV(fullPath.c_str());
	if (sound == nullptr)
	{
		no::Logger::LOG_WARNING("Couldn't load file with path " + fullPath + " \"" + SDL_GetError() + '\"');
	}
	
	return new SoundSDL(sound);
}
