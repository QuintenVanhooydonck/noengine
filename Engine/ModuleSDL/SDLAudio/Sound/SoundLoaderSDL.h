#pragma once
#include "NoEngine/Resources/Internal/IResourceLoader.h"

namespace no
{
	class RendererSDL;
	class ISound;
	struct IDescriptor;

	class SoundLoaderSDL : public IResourceLoader<ISound>
	{
	public:
		SoundLoaderSDL();

		virtual ISound* GetResource(IDescriptor* desc) override;
	};
}