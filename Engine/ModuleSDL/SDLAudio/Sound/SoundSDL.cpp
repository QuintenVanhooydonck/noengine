#include "pch.h"
#include "SoundSDL.h"

#include <SDL_mixer.h>

no::SoundSDL::SoundSDL(Mix_Chunk* sound)
	:m_Sound{ sound }
{
}

no::SoundSDL::~SoundSDL()
{
	if (m_Sound)
	{
		Mix_FreeChunk(m_Sound);
		m_Sound = nullptr;
	}
}

int no::SoundSDL::Play()
{
	if (m_Sound)
	{
		 return Mix_PlayChannel(-1, m_Sound, 0);
	}
	return -1;
}

int no::SoundSDL::Play(int loops)
{
	if (m_Sound)
	{
		return Mix_PlayChannel(-1, m_Sound, loops);
	}
	return -1;
}

void no::SoundSDL::Pause(int id)
{
	if (m_Sound && id != -1)
	{
		Mix_Pause(id);
	}
}

void no::SoundSDL::Resume(int id)
{
	if (m_Sound && id != -1)
	{
		Mix_Resume(id);
	}
}

void no::SoundSDL::Stop(int id)
{
	if (m_Sound && id != -1)
	{
		Mix_HaltChannel(id);
	}
}

void no::SoundSDL::PauseAll()
{
	Mix_Pause(-1);
}

void no::SoundSDL::ResumeAll()
{
	Mix_Resume(-1);
}

void no::SoundSDL::StopAll()
{
	Mix_HaltChannel(-1);
}
