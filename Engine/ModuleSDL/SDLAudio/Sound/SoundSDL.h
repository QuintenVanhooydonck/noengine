#pragma once
#include "NoEngine/Resources/Internal/ISound.h"

struct Mix_Chunk;

namespace no
{
	class SoundSDL : public ISound
	{
	public:
		SoundSDL(Mix_Chunk* sound);
		~SoundSDL();

		virtual int Play() override;
		virtual int Play(int loops) override;
		virtual void Pause(int id) override;
		virtual void Resume(int id) override;
		virtual void Stop(int id) override;

		virtual void PauseAll() override;
		virtual void ResumeAll() override;
		virtual void StopAll() override;

	private:
		Mix_Chunk* m_Sound;
	};
}