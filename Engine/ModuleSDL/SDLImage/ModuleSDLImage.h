#pragma once
#include "NoEngine/Modules/Module.h"

struct SDL_Window;

namespace no
{
	class ModuleSDLImage final : public Module
	{
	public:
		ModuleSDLImage(int screenWidth, int screenHeight, const std::string& windowName);
		~ModuleSDLImage();

	private:
		SDL_Window* m_Window;

		void InitResourceManager();
		void InitRenderer(int screenWidth, int screenHeight, const std::string& windowName);
		void InitSDL();

		virtual void Initialize() override;

	};
}