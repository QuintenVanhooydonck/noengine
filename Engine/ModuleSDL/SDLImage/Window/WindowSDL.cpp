#include "pch.h"
#include "WindowSDL.h"

#include <SDL_image.h>

no::WindowSDL::WindowSDL(SDL_Window* window)
	:m_Window{ window }
{
}

void no::WindowSDL::SetWindowFullscreen(ScreenMode screenMode)
{
	switch (screenMode)
	{
	case no::ScreenMode::fullscreen:
		SDL_SetWindowFullscreen(m_Window, SDL_WINDOW_FULLSCREEN);
		break;
	case no::ScreenMode::borderlessFullscreen:
		SDL_SetWindowFullscreen(m_Window, SDL_WINDOW_FULLSCREEN_DESKTOP);
		break;
	case no::ScreenMode::windowed:
		SDL_SetWindowFullscreen(m_Window, 0);
		break;
	}
}

void no::WindowSDL::SetWindowSize(int width, int height)
{
	SDL_SetWindowSize(m_Window, width, height);
}

void no::WindowSDL::SetWindowResizable(bool resizeable)
{
	SDL_SetWindowResizable(m_Window, resizeable ? SDL_TRUE : SDL_FALSE);
}

void no::WindowSDL::SetWindowTitle(const std::string& name)
{
	SDL_SetWindowTitle(m_Window, name.c_str());
}

void no::WindowSDL::SetVsync(bool)
{
	// SDL hint not working as it sets the hint for the next time the engine is created
}

void no::WindowSDL::Close()
{
	SDL_DestroyWindow(m_Window);
}

glm::vec2 no::WindowSDL::GetWindowSize() const
{
	int x, y;
	SDL_GetWindowSize(m_Window, &x, &y);

	return { float(x), float(y) };
}
