#pragma once
#include "NoEngine/Render/Internal/IWindow.h"

struct SDL_Window;

namespace no
{
	class WindowSDL final : public IWindow
	{
	public:
		WindowSDL(SDL_Window* window);

		virtual void SetWindowFullscreen(ScreenMode screenMode) override;
		virtual void SetWindowSize(int width, int height) override;
		virtual void SetWindowResizable(bool resizeable) override;
		virtual void SetWindowTitle(const std::string& name) override;
		virtual void SetVsync(bool on) override;
		virtual void Close() override;

		SDL_Window* GetWindowHandle() const { return m_Window; }

		virtual glm::vec2 GetWindowSize() const override;

	private:
		SDL_Window* m_Window;
	};
}