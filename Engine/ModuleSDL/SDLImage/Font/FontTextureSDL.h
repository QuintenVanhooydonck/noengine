#pragma once
#include "NoEngine/Resources/Internal/IFontTexture.h"
#include "NoEngine/Math/Vectors.h"

struct SDL_Texture;

namespace no
{
	class RendererSDL;

	class FontTextureSDL : public IFontTexture
	{
	public:
		FontTextureSDL(SDL_Texture* pTexture);
		~FontTextureSDL();

		virtual void Render(float x, float y) override;
		virtual void RenderImmediate(float x, float y) override;

		virtual void SetColor(const glm::vec4& color) override;

	private:
		SDL_Texture* m_pTexture;
		RendererSDL& m_Renderer;
		glm::vec4 m_Color{ 1, 1, 1, 1 };

		void BeforeRender();
	};
}