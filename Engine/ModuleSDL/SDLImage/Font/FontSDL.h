#pragma once
#include "NoEngine/Resources/Internal/IFont.h"
#include <SDL_pixels.h>
#include "NoEngine/Math/Vectors.h"

typedef struct _TTF_Font TTF_Font;

namespace no
{
	class RendererSDL;

	class FontSDL final : public IFont
	{
	public:
		FontSDL(TTF_Font* pFont);

		virtual IFontTexture* GetFontTexture(const std::string& text, size_t fontSize) override;

	private:
		TTF_Font* m_pFont{};
		RendererSDL& m_Renderer;
	};
}