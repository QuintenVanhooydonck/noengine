#include "pch.h"
#include "FontLoaderSDL.h"

// Engine Files
#include "NoEngine/Resources/Internal/Descriptors/FontDescriptor.h"
#include "NoEngine/Resources/Internal/IFont.h"
#include "NoEngine/Resources/Internal/ResourceData.h"
#include "NoEngine/Debug/Logger.h"

// SDL Files
#include <SDL_ttf.h>

// Module Files
#include "FontSDL.h"

namespace no
{

	FontLoaderSDL::FontLoaderSDL()
	{
		if (TTF_Init() != 0)
		{
			throw std::runtime_error(std::string("Failed to load support for fonts: ") + SDL_GetError());
		}
	}

	IFont* FontLoaderSDL::GetResource(IDescriptor* desc)
	{
		auto fontDesc = static_cast<FontDescriptor*>(desc);

		auto fullPath = ResourceData::baseFilePath + fontDesc->m_Path.GetString();

		TTF_Font* pFont = TTF_OpenFont(fullPath.c_str(), int(fontDesc->m_FontSize));
		if (pFont == nullptr)
		{
			no::Logger::LOG_WARNING("Couldn't load file with path " + fullPath + " \"" + SDL_GetError() + '\"');
		}

		return new FontSDL(pFont);
	}
}
