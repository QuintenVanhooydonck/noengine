#include "pch.h"
#include "FontTextureSDL.h"

// Engine Files
#include "NoEngine/Render/Renderer.h"

// SDL Files
#include <SDL_render.h>

// Module Files
#include "../Renderer/RendererSDL.h"


namespace no
{
	FontTextureSDL::FontTextureSDL(SDL_Texture* pTexture)
		:m_pTexture{ pTexture },
		m_Renderer{ *static_cast<RendererSDL*>(Renderer::GetService()) }
	{
	}

	FontTextureSDL::~FontTextureSDL()
	{
		if (m_pTexture)
		{
			SDL_DestroyTexture(m_pTexture);
			m_pTexture = nullptr;
		}
	}

	void FontTextureSDL::Render(float x, float y)
	{
		BeforeRender();
		m_Renderer.RenderTexture(m_pTexture, x, y);
	}

	void FontTextureSDL::RenderImmediate(float x, float y)
	{
		BeforeRender();
		m_Renderer.RenderTextureImmediate(m_pTexture, x, y);
	}

	void FontTextureSDL::SetColor(const glm::vec4& color)
	{
		m_Color = color;
	}

	void FontTextureSDL::BeforeRender()
	{
		SDL_SetTextureColorMod(m_pTexture, Uint8(m_Color.r * 255), Uint8(m_Color.g * 255), Uint8(m_Color.b * 255));
	}
}