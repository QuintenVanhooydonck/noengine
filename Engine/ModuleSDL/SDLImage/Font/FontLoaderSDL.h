#pragma once
#include "NoEngine/Resources/Internal/IResourceLoader.h"

namespace no
{
	class IFont;
	struct IDescriptor;

	class FontLoaderSDL : public IResourceLoader<IFont>
	{
	public:
		FontLoaderSDL();

		IFont* GetResource(IDescriptor* desc);
	};
}