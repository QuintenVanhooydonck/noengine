#include "pch.h"
#include "FontSDL.h"

// Engine Files
#include "NoEngine/Render/Renderer.h"

// SDL Files
#include <SDL_ttf.h>

// Module Files
#include "../Renderer/RendererSDL.h"
#include "../Texture/TextureSDL.h"
#include "FontTextureSDL.h"

namespace no
{
	FontSDL::FontSDL(TTF_Font* pFont)
		:m_pFont{ pFont },
		m_Renderer{ *static_cast<RendererSDL*>(Renderer::GetService()) }
	{
	}

	IFontTexture* FontSDL::GetFontTexture(const std::string& text, size_t fontSize)
	{
		(fontSize);
		if (!m_pFont || text.empty())
			return nullptr;

		SDL_Color renderColor{ 255, 255, 255, 255 };

		const auto surf = TTF_RenderText_Blended(m_pFont, text.c_str(), renderColor);
		if (!surf)
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		
		auto texture = SDL_CreateTextureFromSurface(m_Renderer.GetSDLRenderer(), surf);
		if (!texture)
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());

		SDL_FreeSurface(surf);
		return new FontTextureSDL(texture);
	}
}
