#include "pch.h"
#include "ModuleSDLImage.h"

// Engine Files
#include "NoEngine/Resources/Internal/ResourceManagers/FontManager.h"
#include "NoEngine/Resources/Internal/ResourceManagers/TextureManager.h"
#include "NoEngine/Resources/Internal/IResourceManager.h"
#include "NoEngine/Resources/Internal/ResourceManagerChached.h"
#include "NoEngine/Render/Renderer.h"
#include "NoEngine/Render/Window.h"
#include "NoEngine/Core.h"
#include "SDLEvent/ModuleSDLEvent.h"

// SDL files
#include <SDL_video.h>
#include <SDL.h>

// Module Files
#include "Texture/TextureLoaderSDL.h"
#include "Renderer/RendererSDL.h"
#include "Font/FontLoaderSDL.h"
#include "Window/WindowSDL.h"
#include "Texture/RenderTargetSDL.h"

namespace no
{
	// initialize module
	ModuleSDLImage::ModuleSDLImage(int screenWidth, int screenHeight, const std::string& windowName)
	{
		SDL_SetHint(SDL_HINT_RENDER_DRIVER, "opengl");
		InitSDL();
		InitRenderer(screenWidth, screenHeight, windowName);
		InitResourceManager();
	}

	// unload module
	ModuleSDLImage::~ModuleSDLImage()
	{
		TextureManager::Destroy();
		FontManager::Destroy();

		Window::Destroy();
		Renderer::Destroy();

		SDL_DestroyWindow(m_Window);
	}



	void ModuleSDLImage::InitResourceManager()
	{
		IResourceLoader<ITexture>* textureLoader = new TextureLoaderSDL();
		IResourceManager<ITexture>* textureManager = new ResourceManagerCached<ITexture>(textureLoader);
		TextureManager::Provide(textureManager);

		FontLoaderSDL* fontLoader = new FontLoaderSDL();
		IResourceManager<IFont>* fontManager = new ResourceManagerCached<IFont>(fontLoader);
		FontManager::Provide(fontManager);
	}

	void ModuleSDLImage::InitRenderer(int screenWidth, int screenHeight, const std::string& windowName)
	{
		Uint32 windowFlags = SDL_WINDOW_OPENGL;

		m_Window = SDL_CreateWindow(
			windowName.c_str(),
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			screenWidth,
			screenHeight,
			windowFlags
		);

		if (m_Window == nullptr)
		{
			throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
		}

		RendererSDL* renderer = new RendererSDL();
		renderer->Init(m_Window);
		Renderer::Provide(renderer);
		Window::Provide(new WindowSDL(m_Window));
		SDL_ClearHints();

		RenderTargetSDL::Setup();
	}

	void ModuleSDLImage::InitSDL()
	{
		if (SDL_Init(SDL_INIT_VIDEO) != 0)
		{
			throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
		}
	}

	void ModuleSDLImage::Initialize()
	{
		Core::RequireModule<ModuleSDLEvent>();
	}

}
