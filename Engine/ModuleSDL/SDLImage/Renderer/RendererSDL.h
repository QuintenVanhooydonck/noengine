#pragma once
#include "NoEngine/Render/Internal/IRenderer.h"

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;

namespace no
{
	class RenderTarget;

	class RendererSDL final : public IRenderer
	{
	public:
		~RendererSDL();

		void Init(SDL_Window* window);
		virtual void Render() const override;
		virtual void StartRender() const override;
		virtual void EndRender() const override;


		void RenderTexture(SDL_Texture* texture, float x, float y) const;
		void RenderTexture(SDL_Texture* texture, float x, float y, float width, float height) const;
		void RenderTextureImmediate(SDL_Texture* texture, float x, float y) const;
		void RenderTextureImmediate(SDL_Texture* texture, float x, float y, float width, float height) const;
		
		virtual void Clear() const override;
		
		SDL_Renderer* GetSDLRenderer() const { return m_Renderer; }

		virtual void SetRenderTarget(RenderTarget* renderTarget) override;
		virtual void resetRenderTarget() override;

		
	private:
		SDL_Renderer* m_Renderer{};
		RenderTarget* m_RenderTarget{};
	};
}