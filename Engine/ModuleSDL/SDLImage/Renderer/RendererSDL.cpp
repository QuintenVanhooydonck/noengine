#include "pch.h"
#include "RendererSDL.h"

#include <SDL.h>
#include <SDL_image.h>

//TODO: really need this ?
#include "NoEngine/Scene/SceneManager.h"
#include "NoEngine/Render/Internal/IWindow.h"
#include "NoEngine/Render/Window.h"
#include <SDL_render.h>
#include "SDLImage/Texture/RenderTargetSDL.h"

namespace no
{
	RendererSDL::~RendererSDL()
	{
		if (m_Renderer != nullptr)
		{
			SDL_DestroyRenderer(m_Renderer);
			m_Renderer = nullptr;
		}
	}

	void RendererSDL::Init(SDL_Window* window)
	{
		m_Renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		if (m_Renderer == nullptr)
		{
			throw std::runtime_error(std::string("SDL_CreateRenderer Error: ") + SDL_GetError());
		}
	}

	void RendererSDL::Render() const
	{
		SceneManager::GetInstance().Render();
	}

	void RendererSDL::StartRender() const
	{
		SDL_RenderClear(m_Renderer);
	}

	void RendererSDL::EndRender() const
	{
		SDL_RenderPresent(m_Renderer);
	}

	void RendererSDL::RenderTexture(SDL_Texture* texture, float x, float y) const
	{
		SDL_Rect dst;
		dst.x = static_cast<int>(x);
		dst.y = static_cast<int>(y);
		SDL_QueryTexture(texture, nullptr, nullptr, &dst.w, &dst.h);
		SDL_RenderCopy(GetSDLRenderer(), texture, nullptr, &dst);
	}

	void RendererSDL::RenderTexture(SDL_Texture* texture, float x, float y, float width, float height) const
	{
		SDL_Rect dst;
		dst.x = static_cast<int>(x);
		dst.y = static_cast<int>(y);
		dst.w = static_cast<int>(width);
		dst.h = static_cast<int>(height);
		SDL_RenderCopy(GetSDLRenderer(), texture, nullptr, &dst);
	}

	void RendererSDL::RenderTextureImmediate(SDL_Texture* texture, float x, float y) const
	{
		RenderTexture(texture, x, y);
		SDL_RenderPresent(m_Renderer);
	}

	void RendererSDL::RenderTextureImmediate(SDL_Texture* texture, float x, float y, float width, float height) const
	{
		RenderTexture(texture, x, y, width, height);
		SDL_RenderPresent(m_Renderer);
	}

	void RendererSDL::Clear() const
	{
		SDL_RenderClear(m_Renderer);
	}

	void RendererSDL::SetRenderTarget(RenderTarget* renderTarget)
	{
		if (!renderTarget)
			return;

		if (m_RenderTarget)
		{
			m_RenderTarget->OnRemoved();
			m_RenderTarget = nullptr;
		}

		auto texture = static_cast<RenderTargetSDL*>(renderTarget)->GetTexture();
		SDL_SetRenderTarget(m_Renderer, texture);

		m_RenderTarget = renderTarget;
		m_RenderTarget->OnSet();
	}

	void RendererSDL::resetRenderTarget()
	{
		if (m_RenderTarget)
		{
			m_RenderTarget->OnRemoved();
			m_RenderTarget = nullptr;
		}

		SDL_SetRenderTarget(m_Renderer, nullptr);
	}

}
