#pragma once
#include "NoEngine/Resources/Internal/IResourceLoader.h"

namespace no
{
	class RendererSDL;
	class ITexture;
	struct IDescriptor;

	class TextureLoaderSDL : public IResourceLoader<ITexture>
	{
	public:
		TextureLoaderSDL();
		
		ITexture* GetResource(IDescriptor* desc);

	private:
		RendererSDL& m_Renderer;
	};
}