#pragma once
#include "NoEngine/Resources/Internal/ITexture.h"

struct SDL_Texture;

namespace no
{
	class RendererSDL;

	class TextureSDL : public ITexture
	{
	public:
		TextureSDL(SDL_Texture* texture);
		~TextureSDL();

		virtual void Render(float x, float y) override;

		virtual void RenderImmediate(float x, float y) override;

		virtual void SetColor(const glm::vec4& color) override;

	private:
		SDL_Texture* m_pTexture;
		RendererSDL& m_Renderer;
		glm::uvec4 m_Color{1, 1, 1, 1};

		void BeforeRender();
	};
}