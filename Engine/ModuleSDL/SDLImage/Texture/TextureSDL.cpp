#include "pch.h"
#include "TextureSDL.h"

// Engine Files
#include "NoEngine/Render/Renderer.h"

// SDL Files
#include <SDL_image.h>

// Module Files
#include "SDLImage/Renderer/RendererSDL.h"

namespace no
{
	TextureSDL::TextureSDL(SDL_Texture* texture)
		:m_pTexture{ texture },
		m_Renderer{ *static_cast<RendererSDL*>(Renderer::GetService()) }
	{
	}
	TextureSDL::~TextureSDL()
	{
		if (m_pTexture)
		{
			SDL_DestroyTexture(m_pTexture);
			m_pTexture = nullptr;
		}
	}

	void TextureSDL::Render(float x, float y)
	{
		BeforeRender();
		m_Renderer.RenderTexture(m_pTexture, x, y);
	}

	void TextureSDL::RenderImmediate(float x, float y)
	{
		BeforeRender();
		m_Renderer.RenderTextureImmediate(m_pTexture, x, y);
	}

	void TextureSDL::SetColor(const glm::vec4& color)
	{
		m_Color = color;
	}

	void TextureSDL::BeforeRender()
	{
		SDL_SetTextureColorMod(m_pTexture, Uint8(m_Color.r * 255), Uint8(m_Color.g * 255), Uint8(m_Color.b * 255));
	}

}
