#pragma once
#include "NoEngine/Render/RenderTarget.h"
#include <SDL_opengl.h>

struct SDL_Texture;
struct SDL_Renderer;

namespace no
{
	class RenderTargetSDL final : public RenderTarget
	{
	public:
		static void Setup();
		RenderTargetSDL(const glm::vec2& size, SDL_Renderer* renderer);
		~RenderTargetSDL();

		RenderTargetSDL(RenderTargetSDL&& other) noexcept;
		RenderTargetSDL& operator=(RenderTargetSDL&& other) noexcept;

		virtual void SetTextureSize(const glm::vec2& size) override;
		
		virtual void OnRemoved() override;

		SDL_Texture* GetTexture() const;
		void UpdateGLImage();
		virtual void* GetImagePointer(bool* flipHorizontal, bool* flipVertical) const override 
		{ 
			*flipHorizontal = false;
			*flipVertical = false;
			return (void*)(intptr_t) m_GLTextureID; 
		}

	private:
		SDL_Texture* m_RenderTexture{};
		SDL_Renderer* m_Renderer;
		GLuint m_GLTextureID{};

		void clearTexture();
		static RenderTarget* Create(const glm::vec2& size);
	};
}