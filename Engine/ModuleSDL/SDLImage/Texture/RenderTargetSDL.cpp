#include "pch.h"
#include "RenderTargetSDL.h"
#include <SDL_render.h>
#include "NoEngine/Render/Window.h"
#include "NoEngine/Render/Renderer.h"
#include "SDLImage/Renderer/RendererSDL.h"

void no::RenderTargetSDL::Setup()
{
	SetFactoryFunction(&no::RenderTargetSDL::Create);
}

no::RenderTargetSDL::RenderTargetSDL(const glm::vec2& size, SDL_Renderer* renderer)
	:RenderTarget(size),
	m_Renderer{ renderer }
{
	m_RenderTexture = SDL_CreateTexture(m_Renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_TARGET, int(m_Size.x * 2), int(m_Size.y * 2));
}

no::RenderTargetSDL::~RenderTargetSDL()
{
	clearTexture();
}

no::RenderTargetSDL::RenderTargetSDL(RenderTargetSDL&& other) noexcept
	:RenderTarget(std::move(other)),
	m_RenderTexture{ other.m_RenderTexture },
	m_Renderer{ other.m_Renderer }
{
	other.m_RenderTexture = nullptr;
	other.m_Renderer = nullptr;
}

no::RenderTargetSDL& no::RenderTargetSDL::operator=(RenderTargetSDL&& other) noexcept
{
	RenderTarget::operator=(std::move(other));
	m_RenderTexture = other.m_RenderTexture;
	m_Renderer = other.m_Renderer;

	other.m_RenderTexture = nullptr;
	other.m_Renderer = nullptr;

	return *this;
}

void no::RenderTargetSDL::clearTexture()
{
	if (m_RenderTexture)
	{
		SDL_DestroyTexture(m_RenderTexture);
		m_RenderTexture = nullptr;
	}
}

no::RenderTarget* no::RenderTargetSDL::Create(const glm::vec2& size)
{
	auto renderer = Renderer::GetService();
	auto sdlRenderer = static_cast<RendererSDL*>(renderer)->GetSDLRenderer();
	return new RenderTargetSDL(size, sdlRenderer);
}

void no::RenderTargetSDL::SetTextureSize(const glm::vec2& size)
{
	if (size == m_Size)
		return;

	RenderTarget::SetTextureSize(size);
	clearTexture();
	m_RenderTexture = SDL_CreateTexture(m_Renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_TARGET, int(m_Size.x * 2), int(m_Size.y * 2));
}

SDL_Texture* no::RenderTargetSDL::GetTexture() const
{
	return m_RenderTexture;
}

void no::RenderTargetSDL::UpdateGLImage()
{
	if (m_GLTextureID != 0)
		glDeleteTextures(1, &m_GLTextureID);

	//init surface
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	Uint32 rmask = 0xff000000,
	gmask = 0x00ff0000,
	bmask = 0x0000ff00,
	amask = 0x000000ff;
#else
	Uint32 rmask = 0x000000ff,
	gmask = 0x0000ff00,
	bmask = 0x00ff0000,
	amask = 0xff000000;
#endif
	int width = int(m_Size.x + 0.5f); // round, if not done, screen will sometimes be stretched to 1 pixel smaller
	int height = int(m_Size.y + 0.5f);
	SDL_Surface* surf = SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);

	auto targetBackup = SDL_GetRenderTarget(m_Renderer);
	SDL_SetRenderTarget(m_Renderer, m_RenderTexture);
	auto test = SDL_GetRenderTarget(m_Renderer);
	(test);

	SDL_RenderReadPixels(m_Renderer, &surf->clip_rect, surf->format->format, surf->pixels, surf->pitch);

	// init opengl
	glGenTextures(1, &m_GLTextureID);
	glBindTexture(GL_TEXTURE_2D, m_GLTextureID);

	int Mode = GL_RGB;

	if (surf->format->BytesPerPixel == 4) {
		Mode = GL_RGBA;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, Mode, surf->w, surf->h, 0, Mode, GL_UNSIGNED_BYTE, surf->pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	SDL_SetRenderTarget(m_Renderer, targetBackup);
	SDL_FreeSurface(surf);
}

void no::RenderTargetSDL::OnRemoved()
{
	UpdateGLImage();
}
