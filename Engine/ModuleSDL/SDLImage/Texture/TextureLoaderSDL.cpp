#include "pch.h"
#include "TextureLoaderSDL.h"

// Engine Files
#include "NoEngine/Resources/Internal/Descriptors/TextureDescriptor.h"
#include "NoEngine/Resources/Internal/ResourceData.h"
#include "NoEngine/Render/Renderer.h"
#include "NoEngine/Debug/Logger.h"

// SDL Files
#include <SDL_image.h>

// Module Files
#include "TextureSDL.h"
#include "../Renderer/RendererSDL.h"

namespace no
{
	TextureLoaderSDL::TextureLoaderSDL()
		:m_Renderer{ *static_cast<RendererSDL*>(Renderer::GetService()) }
	{
		if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
		{
			throw std::runtime_error(std::string("Failed to load support for png's: ") + SDL_GetError());
		}

		if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG)
		{
			throw std::runtime_error(std::string("Failed to load support for jpg's: ") + SDL_GetError());
		}
	}

	ITexture* TextureLoaderSDL::GetResource(IDescriptor* desc)
	{
		const auto fullPath = ResourceData::baseFilePath + static_cast<TextureDescriptor*>(desc)->m_Path.GetString();
		auto texture = IMG_LoadTexture(m_Renderer.GetSDLRenderer(), fullPath.c_str());
		if (texture == nullptr)
		{
			no::Logger::LOG_WARNING("Couldn't load file with path " + fullPath + " \"" + SDL_GetError() + '\"');
		}
		return new TextureSDL(texture);
	}
}
