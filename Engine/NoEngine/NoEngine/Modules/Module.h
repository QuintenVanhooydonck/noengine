#pragma once

namespace no
{
	class Module 
	{
	public:
		Module() = default;
		virtual ~Module() = 0 {};

		Module(const Module&) = delete;
		Module(Module&&) = delete;
		Module& operator=(const Module&) = delete;
		Module& operator=(Module&&) = delete;

		// gets called each frame just before updating components
		virtual void Update() {};
		// gets called each frame after rendering the scene
		virtual void Render() {};
		// gets called when the engine is initialized
		virtual void Initialize() {};
	};
}
