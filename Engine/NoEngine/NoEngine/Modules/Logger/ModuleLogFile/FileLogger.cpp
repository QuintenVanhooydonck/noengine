#include "pch.h"
#include "FileLogger.h"
#include <fstream>
#include <functional>

no::FileLogger::FileLogger(const std::string& fileName)
	:m_FileName{ fileName },
	m_PeriodicTask(5, std::bind(&no::FileLogger::WriteToFile, this))
{

}

no::FileLogger::~FileLogger()
{

}

void no::FileLogger::Log(const std::string& info, const std::string& file, const std::string& function, int line)
{
	m_TextBuffer.push_back(info + " [" + function + ':' + std::to_string(line) + "] File\"" + file + '\"');
}

void no::FileLogger::Warning(const std::string& warning, const std::string& file, const std::string& function, int line)
{
	m_TextBuffer.push_back(warning + " [" + function + ':' + std::to_string(line) + "] File\"" + file + '\"');
}

void no::FileLogger::Exception(const std::string& error, const std::string& file, const std::string& function, int line)
{
	m_TextBuffer.push_back(error + " [" + function + ':' + std::to_string(line) + "] File\"" + file + '\"');
}

void no::FileLogger::WriteToFile()
{
	if (m_TextBuffer.empty())
		return;

	std::ofstream logFile(m_FileName, std::fstream::out | std::fstream::app);
	if (logFile.is_open())
	{
		while (m_TextBuffer.size() > 0)
		{
			logFile << m_TextBuffer[0] << '\n';
			m_TextBuffer.pop_front();
		}
	}
}