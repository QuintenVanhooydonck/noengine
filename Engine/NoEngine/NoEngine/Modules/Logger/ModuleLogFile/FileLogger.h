#pragma once
#include <deque>
#include "NoEngine/General/PeriodicTask.h"
#include "NoEngine/Debug/Internal/ILogger.h"

namespace no
{
	class FileLogger final : public ILogger
	{
	public:
		FileLogger(const std::string& fileName);
		~FileLogger();

		virtual void Log(const std::string& info, const std::string& file, const std::string& function, int line) override;
		virtual void Warning(const std::string& warning, const std::string& file, const std::string& function, int line) override;
		virtual void Exception(const std::string& error, const std::string& file, const std::string& function, int line) override;

	private:
		std::string m_FileName;
		std::deque<std::string> m_TextBuffer{};
		PeriodicTask m_PeriodicTask;

		void WriteToFile();
	};
}