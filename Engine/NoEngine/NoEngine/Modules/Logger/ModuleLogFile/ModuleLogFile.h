#pragma once
#include "NoEngine/Modules/Module.h"
#include "NoEngine/Debug/Logger.h"
#include "FileLogger.h"

namespace no
{
	class ModuleLogFile final : public Module
	{
	public:
		ModuleLogFile(const std::string& filePath = "../NoEnine.log")
		{
			m_LoggerID = Logger::Provide(new FileLogger(filePath));
		}

		~ModuleLogFile()
		{
			Logger::Destroy(m_LoggerID);
		}

	private:
		size_t m_LoggerID{};
	};
}