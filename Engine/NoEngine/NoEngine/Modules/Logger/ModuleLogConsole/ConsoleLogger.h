#pragma once
#include "NoEngine/Debug/Internal/ILogger.h"

namespace no
{
	class ConsoleLogger final : public ILogger
	{
	public:
		virtual void Log(const std::string& info, const std::string& file, const std::string& function, int line) override;
		virtual void Warning(const std::string& warning, const std::string& file, const std::string& function, int line) override;
		virtual void Exception(const std::string& error, const std::string& file, const std::string& function, int line) override;

	private:
		enum class Color
		{
			black,
			red,
			green,
			blue,
			yellow,
			cyan,
			magenta,
			white
		};

		void SetColor(Color color) const;
		void ResetColor() const;
	};
}