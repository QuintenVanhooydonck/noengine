#pragma once
#include "NoEngine/Modules/Module.h"
#include "NoEngine/Debug/Logger.h"
#include "ConsoleLogger.h"

namespace no
{
	class ModuleLogConsole final : public Module
	{
	public:
		ModuleLogConsole()
		{
			m_LoggerID = Logger::Provide(new ConsoleLogger());
		}

		~ModuleLogConsole()
		{
			Logger::Destroy(m_LoggerID);
		}

	private:
		size_t m_LoggerID{};
	};
}