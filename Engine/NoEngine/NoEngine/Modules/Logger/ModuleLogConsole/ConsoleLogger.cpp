#include "pch.h"
#include "ConsoleLogger.h"
#ifdef _WIN32
#include <windows.h>
#endif // _WIN32

void no::ConsoleLogger::Log(const std::string& info, const std::string& file, const std::string& function, int line)
{
	UNREFERENCED_PARAMETER(file);

	SetColor(Color::white);

	std::cout << info << " [" << function << ":" << line << "]" << '\n';

	ResetColor();
}

void no::ConsoleLogger::Warning(const std::string& warning, const std::string& file, const std::string& function, int line)
{
	UNREFERENCED_PARAMETER(file);

	SetColor(Color::yellow);

	std::cout << warning << " [" << function << ":" << line << "]" << '\n';

	ResetColor();
}

void no::ConsoleLogger::Exception(const std::string& error, const std::string& file, const std::string& function, int line)
{
	UNREFERENCED_PARAMETER(file);

	SetColor(Color::red);

	std::cout << error << " [" << function << ":" << line << "]" << '\n';

	ResetColor();
}

void no::ConsoleLogger::SetColor(Color color) const
{
#ifdef _WIN32
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	switch (color)
	{
	case Color::black:
		SetConsoleTextAttribute(hConsole, WORD(0));
		break;
	case Color::red:
		SetConsoleTextAttribute(hConsole, WORD(12));
		break;
	case Color::green:
		SetConsoleTextAttribute(hConsole, WORD(10));
		break;
	case Color::blue:
		SetConsoleTextAttribute(hConsole, WORD(9));
		break;
	case Color::yellow:
		SetConsoleTextAttribute(hConsole, WORD(14));
		break;
	case Color::cyan:
		SetConsoleTextAttribute(hConsole, WORD(11));
		break;
	case Color::magenta:
		SetConsoleTextAttribute(hConsole, WORD(13));
		break;
	case Color::white:
		SetConsoleTextAttribute(hConsole, WORD(15));
		break;
	}
#endif // _WIN32
}

void no::ConsoleLogger::ResetColor() const
{
	SetColor(Color::white);
}