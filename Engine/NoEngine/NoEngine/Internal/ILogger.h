#pragma once

namespace no
{
	class ILogger
	{
	public:
		virtual ~ILogger() = default;

		virtual void Log(const std::string& info, const std::string& file, const std::string& function, int line) = 0;
		virtual void Warning(const std::string& warning, const std::string& file, const std::string& function, int line) = 0;
		virtual void Exception(const std::string& error, const std::string& file, const std::string& function, int line) = 0;
	};
}