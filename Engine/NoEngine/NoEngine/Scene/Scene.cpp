#include "pch.h"
#include "Scene.h"
#include "NoEngine/Scene/GameObject.h"

no::Scene::Scene(const std::string& name)
	:m_Name{ name }
{

}

void no::Scene::OnAddGameObject(GameObject* gameObject)
{
	gameObject->SetScene(this);
}