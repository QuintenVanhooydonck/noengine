#pragma once
#include "SceneObjectFunctions.h"

namespace no
{
	class GameObject;

	class Scene final : public SceneObjectFunctions
	{
		friend class GameObject;
		friend class SceneManager;
	public:

		~Scene() {};
		Scene(const Scene&) = delete;
		Scene(Scene&&) = delete;
		Scene& operator=(const Scene&) = delete;
		Scene& operator=(Scene&&) = delete;

	protected:
		virtual void OnAddGameObject(GameObject* gameObject);

	private:
		explicit Scene(const std::string& name);

		std::string m_Name;
	};
}