#pragma once
#include "NoEngine/General/Singleton.h"

namespace no
{
	class Scene;
	class SceneManager final : public Singleton<SceneManager>
	{
		friend class Core;
		friend class Singleton<SceneManager>;
	public:
		Scene* CreateScene(const std::string& name);

		std::vector<Scene*> GetScenes() { return m_Scenes; }

		void Cleanup();

		void Render();

	private:
		SceneManager() = default;
		std::vector<Scene*> m_Scenes{};
		std::vector<Scene*> m_UninitializedScenes{};

		void Update();
		void FixedUpdate();
	};
}
