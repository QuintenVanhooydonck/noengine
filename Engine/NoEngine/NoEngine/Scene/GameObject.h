#pragma once
#include "SceneObjectFunctions.h"
#include "NoEngine/Memory/ReferenceType.h"
#include "NoEngine/Memory/PointerReference.h"
#include <vector>

namespace no
{
	class Scene;
	class BaseComponent;

	class GameObject final : public ReferenceType<GameObject>, public SceneObjectFunctions
	{
		friend class SceneObjectFunctions;
		friend class Scene;
	public:
		static GameObject* Create(Scene* scene = nullptr);
		static GameObject* Create(GameObject* gameObject);

		template <typename T>
		T* AddComponent();
		template <typename T>
		T* GetComponent();
		// returns false and logs an error if it doesn't have the component
		template <typename T>
		bool RequireComponent();
		void Destroy();

		void AttachTo(GameObject* gameObject);
		void AttachTo(Scene* scene = nullptr);

		GameObject* GetParent() const;
		Scene* GetScene() const;

		~GameObject();

		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

	protected:
		void DestroyComponents();

		// gets called when the object is created
		// variables are initialized here
		virtual void Initialize() override;
		// gets called before first frame, other objects are also initialized at this point
		virtual void Start() override;
		// gets called just before destructor gets called
		virtual void OnDestroy() override;
		// gets called every frame
		virtual void Update() override;
		// gets called at the end of every frame
		virtual void LateUpdate() override;
		// gets called at a fixed interval (can be called multiple times in 1 frame)
		virtual void FixedUpdate() override;
		// gets called when graphics should be rendered
		virtual void Render() override;

		virtual void OnAddGameObject(GameObject* gameObject) override;

	private:
		std::vector<BaseComponent*> m_Components{};
		std::vector<BaseComponent*> m_UninitializedComponents{};
		std::vector<BaseComponent*> m_UnstartedComponents{};
		GameObject* m_Parent{};
		Scene* m_Scene{};
		bool m_IsDestroyed{ false };

		GameObject() = default;

		void InitializeComponents();
		void StartComponents();

		void SetParent(GameObject* parent);
		void SetScene(Scene* scene);
		void DetachFromParent();
	};

	template <typename T>
	inline T* GameObject::AddComponent()
	{
		T* newComponent = new T();
		m_UninitializedComponents.push_back(newComponent);
		newComponent->SetGameObject(this);
		newComponent->RootInitialize();
		return newComponent;
	}

	template <typename T>
	inline T* GameObject::GetComponent()
	{
		const type_info& info = typeid(T);
		for (auto component : m_Components)
		{
			if (component && typeid(*component) == info)
				return static_cast<T*>(component);
		}
		return nullptr;
	}

	template <typename T>
	inline bool GameObject::RequireComponent()
	{
#ifdef _DEBUG
		if (!GetComponent<T>())
		{
			//URGENT Logger::LOG_ERROR("Component not found of type" + typeid(T).name());
			return false;
		}
#endif
		return true;
	}
}