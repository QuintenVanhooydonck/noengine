#include "pch.h"
#include "SceneManager.h"
#include "NoEngine/Scene/Scene.h"

no::Scene* no::SceneManager::CreateScene(const std::string& name)
{
	Scene* newScene = new Scene(name);
	m_Scenes.push_back(newScene);
	return newScene;
}

void no::SceneManager::Cleanup()
{
	for (auto scene : m_Scenes)
	{
		scene->OnDestroy();
		delete scene;
	}
	m_Scenes.clear();
}

void no::SceneManager::Update()
{
	for (auto scene : m_Scenes)
	{
		scene->Update();
	}

	for (auto scene : m_Scenes)
	{
		scene->LateUpdate();
	}
}

void no::SceneManager::FixedUpdate()
{
	for (auto scene : m_Scenes)
	{
		scene->FixedUpdate();
	}
}

void no::SceneManager::Render()
{
	for (auto scene : m_Scenes)
	{
		scene->Render();
	}
}