#pragma once

namespace no
{
	class GameObject;

	class SceneObjectFunctions
	{
		friend class GameObject;
	public:
		virtual ~SceneObjectFunctions() = 0;
		
	protected:
		std::vector<GameObject*> m_GameObjects{};
		std::vector<GameObject*> m_NewGameObjects{};
		std::vector<GameObject*> m_DetachedObjects{};

		void DestroyGameObjects();

		// gets called when the object is created
		// variables are initialized here
		virtual void Initialize();
		// gets called before first frame, other objects are also initialized at this point
		virtual void Start();
		// gets called just before destructor gets called
		virtual void OnDestroy();
		// gets called every frame
		virtual void Update();
		// gets called at the end of every frame
		virtual void LateUpdate();
		// gets called at a fixed interval (can be called multiple times in 1 frame)
		virtual void FixedUpdate();
		// gets called when graphics should be rendered
		virtual void Render();

		virtual void OnAddGameObject(GameObject* gameObject) = 0;

		void Add(GameObject* gameObject);
		void Remove(GameObject* gameObject);

	private:
		void AttachGameObjects();
		void DetachGameObjects();
	};
}