#include "pch.h"
#include "GameObject.h"
#include "Scene.h"
#include "SceneManager.h"
#include "NoEngine/Components/BaseComponent.h"

no::GameObject* no::GameObject::Create(Scene* scene)
{
	if (!scene)
		scene = SceneManager::GetInstance().GetScenes()[0];

	auto newGameObject = new GameObject();
	newGameObject->AttachTo(scene);
	return newGameObject;
}

no::GameObject* no::GameObject::Create(GameObject* gameObject)
{
	auto newGameObject = new GameObject();
	newGameObject->AttachTo(gameObject);
	return newGameObject;
}

no::GameObject::~GameObject()
{
}

void no::GameObject::Destroy()
{
	m_IsDestroyed = true;
}

void no::GameObject::AttachTo(GameObject* gameObject)
{
	if (gameObject)
	{
		DetachFromParent();
		gameObject->Add(this);
	}
}

void no::GameObject::AttachTo(Scene* scene)
{
	if (!scene)
		scene = SceneManager::GetInstance().GetScenes()[0];

	DetachFromParent();
	scene->Add(this);
}

void no::GameObject::DetachFromParent()
{
	if (m_Parent)
	{
		m_Parent->Remove(this);
		m_Parent = nullptr;
	}
	if (m_Scene)
	{
		m_Scene->Remove(this);
		m_Scene = nullptr;
	}
}

void no::GameObject::OnAddGameObject(GameObject* gameObject)
{
	gameObject->SetParent(this);
}

void no::GameObject::Initialize()
{
	SceneObjectFunctions::Initialize();
	// do nothing
}

void no::GameObject::Start()
{
	SceneObjectFunctions::Start();
	// do nothing
}

void no::GameObject::OnDestroy()
{
	SceneObjectFunctions::OnDestroy();

	for (auto component : m_Components)
	{
		if (!component)
			continue;

		component->OnDestroy();
		delete component;
	}
	m_Components.clear();

	for (auto component : m_UninitializedComponents)
	{
		if (!component)
			continue;

		component->OnDestroy();
		delete component;
	}
	m_UninitializedComponents.clear();
}

void no::GameObject::Update()
{
	SceneObjectFunctions::Update();

	InitializeComponents();
	StartComponents();

	for (auto& component : m_Components)
	{
		component->RootUpdate();
	}

	DestroyComponents();
}

void no::GameObject::LateUpdate()
{
	SceneObjectFunctions::LateUpdate();

	for (auto& component : m_Components)
	{
		component->RootLateUpdate();
	}
}

void no::GameObject::FixedUpdate()
{
	SceneObjectFunctions::FixedUpdate();

	for (auto& component : m_Components)
	{
		component->RootFixedUpdate();
	}
}

void no::GameObject::Render()
{
	SceneObjectFunctions::Render();

	for (auto& component : m_Components)
	{
		component->RootRender();
	}
}

void no::GameObject::InitializeComponents()
{
	for (auto& component : m_UninitializedComponents)
	{
		m_UnstartedComponents.push_back(component);
		m_Components.push_back(component);
		component->RootInitialize();
	}
	m_UninitializedComponents.clear();
}

void no::GameObject::StartComponents()
{
	for (auto& component : m_UnstartedComponents)
	{
		component->RootStart();
	}
	m_UnstartedComponents.clear();
}

void no::GameObject::SetParent(GameObject* parent)
{
	if (!parent)
		return;

	m_Scene = nullptr;
	m_Parent = parent;
}

void no::GameObject::SetScene(Scene* scene)
{
	if (!scene)
		return;

	m_Parent = nullptr;
	m_Scene = scene;
}

void no::GameObject::DestroyComponents()
{
	for (size_t i = 0; i < m_Components.size(); i++)
	{
		if (m_Components[i]->m_IsDestroyed)
		{
			m_Components[i]->OnDestroy();
			delete m_Components[i];
			std::swap(m_Components[i], m_Components.back());
			m_Components.pop_back();
			--i;
		}
	}
}

no::GameObject* no::GameObject::GetParent() const
{
	return m_Parent;
}

no::Scene* no::GameObject::GetScene() const
{
	if (m_Scene)
		return m_Scene;
	else
	{
		return m_Parent->GetScene();
	}
}
