#include "pch.h"
#include "SceneObjectFunctions.h"
#include "GameObject.h"

no::SceneObjectFunctions::~SceneObjectFunctions()
{
	OnDestroy();
}

void no::SceneObjectFunctions::Add(GameObject* gameObject)
{
	m_NewGameObjects.push_back(gameObject);
}

void no::SceneObjectFunctions::Remove(GameObject* gameObject)
{
	m_DetachedObjects.push_back(gameObject);
}

void no::SceneObjectFunctions::AttachGameObjects()
{
	for (auto gameObject : m_NewGameObjects)
	{
		OnAddGameObject(gameObject);
		//gameObject->Initialize();
		m_GameObjects.push_back(gameObject);
	}
	m_NewGameObjects.clear();
}

void no::SceneObjectFunctions::DetachGameObjects()
{
	for (auto gameObject : m_DetachedObjects)
	{
		auto it = std::find(m_GameObjects.begin(), m_GameObjects.end(), gameObject);
		if (it != m_GameObjects.end())
		{
			std::swap(*it, m_GameObjects.back());
			m_GameObjects.pop_back();
		}
	}
	m_DetachedObjects.clear();
}

void no::SceneObjectFunctions::DestroyGameObjects()
{
	for (size_t i = 0; i < m_GameObjects.size(); ++i)
	{
		if (m_GameObjects[i]->m_IsDestroyed)
		{
			delete m_GameObjects[i];
			std::swap(m_GameObjects[i], m_GameObjects.back());
			m_GameObjects.pop_back();
			--i;
		}
	}
}

void no::SceneObjectFunctions::Initialize()
{
	// do nothing
}

void no::SceneObjectFunctions::Start()
{
	// do nothing
}

void no::SceneObjectFunctions::OnDestroy()
{
	for (auto gameObject : m_GameObjects)
	{
		if (!gameObject)
			continue;

		gameObject->OnDestroy();
		delete gameObject;
	}
	m_GameObjects.clear();

	for (auto gameObject : m_NewGameObjects)
	{
		if (!gameObject)
			continue;

		gameObject->OnDestroy();
		delete gameObject;
	}
	m_NewGameObjects.clear();
}

void no::SceneObjectFunctions::Update()
{
	DetachGameObjects();

	AttachGameObjects();

	DestroyGameObjects();

	for (auto gameObject : m_GameObjects)
	{
		gameObject->Update();
	}
}

void no::SceneObjectFunctions::LateUpdate()
{
	for (auto gameObject : m_GameObjects)
	{
		gameObject->LateUpdate();
	}
}

void no::SceneObjectFunctions::FixedUpdate()
{
	for (auto gameObject : m_GameObjects)
	{
		gameObject->FixedUpdate();
	}
}

void no::SceneObjectFunctions::Render()
{
	for (auto gameObject : m_GameObjects)
	{
		gameObject->Render();
	}
}