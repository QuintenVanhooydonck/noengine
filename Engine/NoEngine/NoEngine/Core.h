#pragma once
#include <iostream>

namespace no
{
	class Module;

	class Core
	{
	public:
		Core(const std::vector<Module*>& modules = std::vector<Module*>{}, const std::string& baseDataPath = "");
		~Core();

		Core(const Core&) = delete;
		Core(Core&&) noexcept;
		Core& operator=(const Core&) = delete;
		Core& operator=(Core&&) noexcept;

		void Initialize();
		// Call cleanup if run was never called
		void Cleanup();

		void Run();

		void Step(float deltaTime);
		void Render() const;
		void UpdateModules() const;
		void RenderModules() const;

		static void Quit();
		static bool IsQuitting();

		template <class T>
		static T* GetModule();
		template <class T>
		// engine checks if module type is available, if not throw an error
		static void RequireModule();

	private:
		std::vector<Module*> m_Modules{};
		static Core* m_pEngine;
		bool m_Quit{ false };

		void QuitInternal();

	};

	template <class T>
	T* Core::GetModule()
	{
		if (!m_pEngine)
		{
			std::cout << "Engine was not initialized yet!\n";
			std::abort();
		}

		const type_info& info = typeid(T);
		for (auto engineModule : m_pEngine->m_Modules)
		{
			if (engineModule && typeid(*engineModule) == info)
			{
				return static_cast<T*>(engineModule);
			}
		}

		std::cout << "Engine didn't have module with type: " << info.name() << '\n';
		std::abort();
	}

	template<class T>
	void Core::RequireModule()
	{
		if (!m_pEngine)
		{
			std::cout << "Engine was not initialized yet!\n";
			std::abort();
		}

		const type_info& info = typeid(T);
		for (auto engineModule : m_pEngine->m_Modules)
		{
			if (engineModule && typeid(*engineModule) == info)
			{
				return;
			}
		}

		std::cout << "Engine didn't have module with type: " << info.name() << '\n';
		std::abort();
	}
}