#pragma once
#include "NoEngine/Math/Vectors.h"
#include <functional>
namespace no
{
	class RenderTarget
	{
	public:
		static RenderTarget* Create(const glm::vec2& size);

		virtual ~RenderTarget() = default;

		RenderTarget(const RenderTarget&) = delete;
		RenderTarget(RenderTarget&&) = default;
		RenderTarget& operator=(const RenderTarget&) = delete;
		RenderTarget& operator=(RenderTarget&&) = default;
		
		virtual void* GetImagePointer(bool* flipHorizontal, bool* flipVertical) const { 
			(flipHorizontal);
			(flipVertical);
			return 0; 
		}

		virtual void SetTextureSize(const glm::vec2& size)
		{
			m_Size = size;
		};

		virtual void OnSet() {}
		virtual void OnRemoved() {}

	protected:
		glm::vec2 m_Size;
		
		RenderTarget(const glm::vec2& size)
			:m_Size{ size }
		{
		}

		static void SetFactoryFunction(const std::function < RenderTarget*(const glm::vec2& size)>& createFunction);

	private:
		static std::function<RenderTarget*(const glm::vec2& size)> m_CreateFunction;
	};
}