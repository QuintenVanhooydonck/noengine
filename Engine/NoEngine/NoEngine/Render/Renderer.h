#pragma once
#include "NoEngine/General/Locator.h"
#include "Internal/IRenderer.h"

namespace no
{
	class Renderer : public Locator<IRenderer> 
	{
	public:
		static void Render()
		{
			auto renderer = GetService();
			if (renderer)
				renderer->Render();
		}

		static void StartRender()
		{
			auto renderer = GetService();
			if (renderer)
				renderer->StartRender();
		}

		static void EndRender()
		{
			auto renderer = GetService();
			if (renderer)
				renderer->EndRender();
		}

		static void Clear()
		{
			auto renderer = GetService();
			if (renderer)
				renderer->Clear();
		}
	};
}