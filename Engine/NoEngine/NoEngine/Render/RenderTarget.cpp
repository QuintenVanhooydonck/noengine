#include "pch.h"
#include "RenderTarget.h"


std::function<no::RenderTarget * (const glm::vec2 & size)> no::RenderTarget::m_CreateFunction{};

no::RenderTarget* no::RenderTarget::Create(const glm::vec2& size)
{
	return m_CreateFunction(size);
}

void no::RenderTarget::SetFactoryFunction(const std::function<RenderTarget*(const glm::vec2& size)>& createFunction)
{
	m_CreateFunction = createFunction;
}

