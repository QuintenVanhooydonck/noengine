#pragma once
#include "NoEngine/Render/RenderTarget.h"

namespace no
{
	class IRenderer
	{
	public:

		virtual void StartRender() const = 0;
		virtual void EndRender() const = 0;
		virtual void Render() const = 0;
		virtual void Clear() const = 0;

		virtual void SetRenderTarget(RenderTarget* renderTarget) = 0;
		virtual void resetRenderTarget() = 0;
	};
}