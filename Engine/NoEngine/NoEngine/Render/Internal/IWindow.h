#pragma once
#include <string>
#include "NoEngine/Math/Vectors.h"

namespace no
{
	enum class ScreenMode
	{
		fullscreen,
		borderlessFullscreen,
		windowed
	};

	class IWindow
	{
	public:
		virtual ~IWindow() = default;

		virtual void SetWindowFullscreen(ScreenMode screenMode) = 0;
		virtual void SetWindowSize(int width, int height) = 0;
		virtual void SetWindowResizable(bool) = 0;
		virtual void SetWindowTitle(const std::string& name) = 0;
		virtual void SetVsync(bool on) = 0;
		virtual void Close() = 0;

		virtual glm::vec2 GetWindowSize() const = 0;
	};
}