#include "pch.h"
#include "Window.h"

void no::Window::SetWindowFullscreen(ScreenMode screenMode)
{
	auto service = GetService();
	if (service)
		service->SetWindowFullscreen(screenMode);
}

void no::Window::SetWindowSize(int width, int height)
{
	auto service = GetService();
	if (service)
		service->SetWindowSize(width, height);
}

void no::Window::SetWindowTitle(const std::string& name)
{
	auto service = GetService();
	if (service)
		service->SetWindowTitle(name);
}

void no::Window::SetVsync(bool on)
{
	auto service = GetService();
	if (service)
		service->SetVsync(on);
}

void no::Window::Close()
{
	auto service = GetService();
	if (service)
		service->Close();
}
