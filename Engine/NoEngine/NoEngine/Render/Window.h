#pragma once
#include "Internal/IWindow.h"
#include "NoEngine/General/Locator.h"

namespace no
{
	class Window : public Locator<IWindow> 
	{
	public:
		static void SetWindowFullscreen(ScreenMode screenMode);
		static void SetWindowSize(int width, int height);
		static void SetWindowTitle(const std::string& name);
		static void SetVsync(bool on);
		static void Close();
	};
}