#include "pch.h"
#include "Core.h"

#include <chrono>

#include "NoEngine/Scene/SceneManager.h"
#include "NoEngine/Modules/Module.h"
#include "NoEngine/General/Time.h"
#include "NoEngine/Resources/Internal/ResourceData.h"
#include "NoEngine/Render/Renderer.h"

#include <thread>

namespace no
{
	Core* Core::m_pEngine{};

	void Core::Initialize()
	{
		if (m_pEngine)
			throw "Can't run multiple instances of the engine";

		m_pEngine = this;

		Time::ResetTime();

		for (auto engineModule : m_Modules)
		{
			engineModule->Initialize();
		}
	}

	Core::Core(const std::vector<Module*>& modules, const std::string& baseDataPath)
		:m_Modules{ modules }
	{
		ResourceData::baseFilePath = baseDataPath;
		(baseDataPath);
	}

	Core::~Core()
	{
		Cleanup();
	}

	Core::Core(Core&& other) noexcept
		:m_Modules{ other.m_Modules }
	{
		other.m_Modules.clear();
		if (&other == m_pEngine)
			m_pEngine = this;
	}

	Core& Core::operator=(Core&& other) noexcept
	{
		Cleanup();
		m_Modules = other.m_Modules;

		other.m_Modules.clear();
		if (&other == m_pEngine)
			m_pEngine = this;

		return *this;
	}

	void Core::Cleanup()
	{
		SceneManager::GetInstance().Cleanup();

		for (auto engineModule : m_Modules)
		{
			delete engineModule;
		}
		m_Modules.clear();

		if(m_pEngine == this)
			m_pEngine = nullptr;
	}

	void Core::Run()
	{
		auto lastTime = std::chrono::high_resolution_clock::now();

		long long minFrameTimeMs = 1;

		while (!m_Quit)
		{
			const auto startTime = std::chrono::high_resolution_clock::now();
			float deltaTime{ std::chrono::duration<float>(startTime - lastTime).count() };

			Step(deltaTime);
			UpdateModules();
			Renderer::StartRender();
			Render();
			RenderModules();
			Renderer::EndRender();

			lastTime = startTime;
			std::this_thread::sleep_until(startTime + std::chrono::milliseconds(minFrameTimeMs));
		}
	}

	void Core::Step(float deltaTime)
	{
		Time::SetDeltaTime(deltaTime);

		SceneManager::GetInstance().Update();
	}

	void Core::Render() const
	{
		Renderer::Render();
	}

	void Core::UpdateModules() const
	{
		for (auto engineModule : m_Modules)
		{
			engineModule->Update();
		}
	}

	void Core::RenderModules() const
	{
		for (auto engineModule : m_Modules)
		{
			engineModule->Render();
		}
	}

	void Core::Quit()
	{
		if (m_pEngine)
			m_pEngine->QuitInternal();
	}

	bool Core::IsQuitting()
	{
		if (m_pEngine)
			return m_pEngine->m_Quit;
		return true;
	}

	void Core::QuitInternal()
	{
		m_Quit = true;
	}
}
