#pragma once
#include "NoEngine/Math/Vectors.h"
#include "Internal/Descriptors/FontDescriptor.h"

namespace no
{
	class IFont;
	class IFontTexture;
	
	class Font
	{
	public:
		Font(const std::string& path, const std::string& text, size_t fontSize = 20, const glm::vec4& color = { 1, 1, 1, 1 });
		~Font();

		Font(const Font& other);
		Font(Font&& other) noexcept;
		Font& operator=(const Font& other);
		Font& operator=(Font&& other) noexcept;

		void SetText(const std::string& text);
		void SetColor(const glm::vec4& color);
		void SetSize(size_t fontSize);
		void SetFontPath(const std::string& path);

		void Render(float x, float y);
		void RenderImmediate(float x, float y);

	private:
		IFont* m_pFont{};
		IFontTexture* m_pFontTexture{};

		FontDescriptor m_FontDesc;
		std::string m_Text;
		glm::vec4 m_Color{ 1, 1, 1, 1 };

		void LoadFont();
		void LoadFontTexture();
		void Cleanup();
	};
}