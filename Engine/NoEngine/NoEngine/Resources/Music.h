#pragma once

namespace no
{
	class IMusic;

	class Music
	{
	public:
		Music(const std::string& path);
		~Music();

		Music(const Music& other);
		Music(Music&& other) noexcept;
		Music& operator=(const Music& other);
		Music& operator=(Music&& other) noexcept;

		void Play();
		void Pause();
		void Resume();
		void Stop();

	private:
		IMusic* m_pMusic;

		void Cleanup();
	};
}
