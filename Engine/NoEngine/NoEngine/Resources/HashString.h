#pragma once
#include <string>

class HashString
{
public:
	HashString() = default;
	HashString(const std::string& text);

	HashString& operator=(const std::string& text);
	HashString& operator=(std::string&& text);

	const std::string& GetString() const;
	const size_t& GetHash() const;
private:
	static std::hash<std::string> m_Hasher;
	std::string m_Text;
	size_t m_Hash;
};