#pragma once

namespace no
{
	struct IDescriptor;

	template <typename T>
	class IResourceLoader
	{
	public:
		virtual T* GetResource(IDescriptor* desc) = 0;
	};
}