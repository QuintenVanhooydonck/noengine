#pragma once
#include "IDescriptor.h"
#include "NoEngine/Resources/HashString.h"
#include "NoEngine/Math/Vectors.h"

namespace no
{
	struct FontDescriptor final : public IDescriptor
	{
	public:
		virtual size_t GetHash() const override { return m_Path.GetHash() + m_FontSize; }
		HashString m_Path;
		size_t m_FontSize;
	};
}
