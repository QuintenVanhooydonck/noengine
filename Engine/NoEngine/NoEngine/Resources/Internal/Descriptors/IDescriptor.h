#pragma once

namespace no
{
	struct IDescriptor
	{
	public:
		virtual size_t GetHash() const = 0;
	};
}