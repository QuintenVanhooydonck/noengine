#pragma once
#include "IDescriptor.h"
#include "NoEngine/Resources/HashString.h"

namespace no
{
	struct MusicDescriptor final : public IDescriptor
	{
	public:
		virtual size_t GetHash() const override { return m_Path.GetHash(); }
		HashString m_Path;
	};
}