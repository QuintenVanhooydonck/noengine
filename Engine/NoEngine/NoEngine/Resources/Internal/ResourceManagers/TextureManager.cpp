#include "pch.h"
#include "TextureManager.h"
#include "NoEngine/Resources/Internal/Descriptors/TextureDescriptor.h"

no::ITexture* no::TextureManager::GetTexture(TextureDescriptor* textureDesc)
{
	auto manager = GetService();
	if (manager)
		return manager->GetResource(textureDesc);
	return nullptr;
}

no::ITexture* no::TextureManager::AddReference(ITexture* texture)
{
	auto manager = GetService();
	if (manager)
		return manager->AddReference(texture);
	return nullptr;
}

void no::TextureManager::ReleaseTexture(ITexture* texture)
{
	auto manager = GetService();
	if (manager)
		manager->ReleaseResource(texture);
}
