#pragma once
#include "NoEngine/General/Locator.h"
#include "NoEngine/Resources/Internal/IResourceManager.h"
#include "NoEngine/Resources/Internal/IMusic.h"

namespace no
{
	struct MusicDescriptor;

	class MusicManager final : public Locator<IResourceManager<IMusic>>
	{
	public:
		static IMusic* GetMusic(MusicDescriptor* musicDesc);
		static IMusic* AddReference(IMusic* music);
		static void ReleaseMusic(IMusic* music);
	};
}