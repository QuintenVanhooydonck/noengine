#pragma once
#include "NoEngine/General/Locator.h"
#include "NoEngine/Resources/Internal/IResourceManager.h"
#include "NoEngine/Resources/Internal/ISound.h"


namespace no
{
	struct SoundDescriptor;

	class SoundManager final : public Locator<IResourceManager<ISound>>
	{
	public:
		static ISound* GetSound(SoundDescriptor* soundDesc);
		static ISound* AddReference(ISound* sound);
		static void ReleaseSound(ISound* sound);
	};
}
