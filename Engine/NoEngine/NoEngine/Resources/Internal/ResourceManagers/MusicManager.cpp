#include "pch.h"
#include "MusicManager.h"
#include "NoEngine/Resources/Internal/Descriptors/MusicDescriptor.h"

no::IMusic* no::MusicManager::GetMusic(MusicDescriptor* musicDesc)
{
	auto manager = GetService();
	if (manager)
		return manager->GetResource(musicDesc);
	return nullptr;
}

no::IMusic* no::MusicManager::AddReference(IMusic* music)
{
	auto manager = GetService();
	if (manager)
		return manager->AddReference(music);
	return nullptr;
}

void no::MusicManager::ReleaseMusic(IMusic* music)
{
	auto manager = GetService();
	if (manager)
		manager->ReleaseResource(music);
}
