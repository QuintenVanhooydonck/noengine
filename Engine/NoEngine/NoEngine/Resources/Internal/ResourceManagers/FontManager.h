#pragma once
#include "NoEngine/General/Locator.h"
#include "NoEngine/Resources/Internal/IResourceManager.h"
#include "NoEngine/Resources/Internal/IFont.h"

namespace no
{
	struct FontDescriptor;

	class FontManager final : public Locator<IResourceManager<IFont>>
	{
	public:
		static IFont* GetFont(FontDescriptor* fontDesc);
		static IFont* AddReference(IFont* font);
		static void ReleaseFont(IFont* font);
	};
}
