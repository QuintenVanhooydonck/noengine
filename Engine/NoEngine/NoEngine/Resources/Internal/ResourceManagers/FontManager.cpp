#include "pch.h"
#include "FontManager.h"
#include "NoEngine/Resources/Internal/Descriptors/FontDescriptor.h"

no::IFont* no::FontManager::GetFont(FontDescriptor* fontDesc)
{
	auto manager = GetService();
	if (manager)
		return manager->GetResource(fontDesc);
	return nullptr;
}

no::IFont* no::FontManager::AddReference(IFont* font)
{
	auto manager = GetService();
	if (manager)
		return manager->AddReference(font);
	return nullptr;
}

void no::FontManager::ReleaseFont(IFont* font)
{
	auto manager = GetService();
	if (manager)
		manager->ReleaseResource(font);
}
