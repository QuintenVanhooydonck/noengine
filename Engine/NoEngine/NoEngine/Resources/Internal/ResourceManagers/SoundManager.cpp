#include "pch.h"
#include "SoundManager.h"
#include "NoEngine/Resources/Internal/Descriptors/SoundDescriptor.h"

no::ISound* no::SoundManager::GetSound(SoundDescriptor* soundDesc)
{
	auto manager = GetService();
	if (manager)
		return manager->GetResource(soundDesc);
	return nullptr;
}

no::ISound* no::SoundManager::AddReference(ISound* sound)
{
	auto manager = GetService();
	if (manager)
		return manager->AddReference(sound);
	
	return nullptr;
}

void no::SoundManager::ReleaseSound(ISound* sound)
{
	auto manager = GetService();
	if (manager)
		manager->ReleaseResource(sound);
}
