#pragma once
#include "NoEngine/General/Locator.h"
#include "NoEngine/Resources/Internal/IResourceManager.h"
#include "NoEngine/Resources/Internal/ITexture.h"

namespace no
{
	struct TextureDescriptor;

	class TextureManager final : public Locator<IResourceManager<ITexture>>
	{
	public:
		static ITexture* GetTexture(TextureDescriptor* textureDesc);
		static ITexture* AddReference(ITexture* texture);
		static void ReleaseTexture(ITexture* texture);
	};
}
