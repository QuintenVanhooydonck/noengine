#pragma once
#include "Descriptors/IDescriptor.h"

namespace no
{
	template <typename T>
	class IResourceManager
	{
	public:
		virtual ~IResourceManager() = 0 {};

		virtual T* GetResource(IDescriptor* desc) = 0;
		virtual T* AddReference(T* pResource) = 0;
		virtual void ReleaseResource(T* pResource) = 0;
	};
}