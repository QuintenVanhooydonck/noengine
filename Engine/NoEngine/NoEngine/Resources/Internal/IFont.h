#pragma once
#include "NoEngine/Math/Vectors.h"
#include <string>


namespace no
{
	class IFontTexture;
	
	class IFont
	{
	public:
		IFont() = default;
		virtual ~IFont() = default;

		virtual IFontTexture* GetFontTexture(const std::string& text, size_t fontSize = 20U) = 0;
	};
}