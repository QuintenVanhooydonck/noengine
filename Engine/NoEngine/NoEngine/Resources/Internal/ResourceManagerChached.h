#pragma once
#include <unordered_map>

#include "IResourceManager.h"
#include "IResourceLoader.h"

namespace no
{
	template <typename T>
	class ResourceManagerCached : public IResourceManager<T>
	{
	private:
		struct CacheData
		{
			T* m_pResource;
			int m_NumRef;

			CacheData(T* pResource, int numRef = 1)
				:m_pResource{ pResource },
				m_NumRef{ numRef }
			{}
		};
	public:
		ResourceManagerCached(IResourceLoader<T>* loader);
		~ResourceManagerCached();
		virtual T* GetResource(IDescriptor* desc) override;
		virtual T* AddReference(T* pResource) override;
		virtual void ReleaseResource(T* pResource) override;

	private:
		std::unordered_map<size_t, CacheData> m_ResourceCache{};
		IResourceLoader<T>* m_pResourceLoader;

		bool GetResourceIterator(typename std::unordered_map<size_t, CacheData>::iterator& iter, T* pResource)
		{
			iter = std::find_if(m_ResourceCache.begin(), m_ResourceCache.end(), [pResource](auto pair)
				{
					return pair.second.m_pResource == pResource;
				});

			return iter != m_ResourceCache.end();
		}
	};
}



template <typename T>
no::ResourceManagerCached<T>::ResourceManagerCached(IResourceLoader<T>* loader)
	: m_pResourceLoader{ loader }
{
}

template <typename T>
no::ResourceManagerCached<T>::~ResourceManagerCached()
{
	if (m_pResourceLoader)
	{
		delete m_pResourceLoader;
		m_pResourceLoader = nullptr;
	}
}

template <typename T>
T* no::ResourceManagerCached<T>::GetResource(IDescriptor* desc)
{
	auto resourceIt = m_ResourceCache.find(desc->GetHash());

	if (resourceIt == m_ResourceCache.end())
	{
		auto newResource = m_pResourceLoader->GetResource(desc);

		m_ResourceCache.insert(std::make_pair(desc->GetHash(), CacheData(newResource)));

		return newResource;
	}

	++resourceIt->second.m_NumRef;
	return resourceIt->second.m_pResource;
}

template<typename T>
inline T* no::ResourceManagerCached<T>::AddReference(T* pResource)
{
	if (!pResource)
		return nullptr;

	typename std::unordered_map<size_t, CacheData>::iterator resourceIt;
	if (!GetResourceIterator(resourceIt, pResource))
		return nullptr;

	++resourceIt->second.m_NumRef;

	return pResource;
}

template <typename T>
void no::ResourceManagerCached<T>::ReleaseResource(T* pResource)
{
	auto resourceIt = std::find_if(m_ResourceCache.begin(), m_ResourceCache.end(), [pResource](auto pair)
		{
			return pair.second.m_pResource == pResource;
		});

	if (resourceIt == m_ResourceCache.end())
		return;

	--resourceIt->second.m_NumRef;

	if (resourceIt->second.m_NumRef <= 0)
	{
		delete resourceIt->second.m_pResource;
		m_ResourceCache.erase(resourceIt->first);
	}
}