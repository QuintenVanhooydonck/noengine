#pragma once

namespace no
{
	class ISound
	{
	public:
		// returns id to pause sound afterwards or -1 if the sound didn't play
		virtual int Play() = 0;
		virtual int Play(int loops) = 0;
		virtual void Pause(int id) = 0;
		virtual void Resume(int id) = 0;
		virtual void Stop(int id) = 0;

		// TODO make it generally accessible in locator
		virtual void PauseAll() = 0;
		virtual void ResumeAll() = 0;
		virtual void StopAll() = 0;
	};
}