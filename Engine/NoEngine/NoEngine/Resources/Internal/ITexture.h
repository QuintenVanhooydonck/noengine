#pragma once
#include "NoEngine/Math/Vectors.h"

namespace no
{
	class ITexture
	{
	public:
		ITexture() = default;
		virtual ~ITexture() = default;
		virtual void Render(float x, float y) = 0;

		virtual void RenderImmediate(float x, float y) = 0;

		// needs to be called just before rendering to modulate color
		virtual void SetColor(const glm::vec4& color) = 0;
	};
}