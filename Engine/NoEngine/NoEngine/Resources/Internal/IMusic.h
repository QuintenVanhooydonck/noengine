#pragma once

namespace no
{
	class IMusic
	{
	public:
		virtual void Play() = 0;
		virtual void Pause() = 0;
		virtual void Resume() = 0;
		virtual void Stop() = 0;
	};
}