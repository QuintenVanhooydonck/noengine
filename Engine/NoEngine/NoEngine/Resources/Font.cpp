#include "pch.h"
#include "Font.h"
#include "Internal/ResourceManagers/FontManager.h"
#include "Internal/IFontTexture.h"

namespace no
{
	Font::Font(const std::string& path, const std::string& text, size_t fontSize, const glm::vec4& color)
		:m_FontDesc{},
		m_Text{ text },
		m_Color{ color }
	{
		m_FontDesc.m_Path = path;
		m_FontDesc.m_FontSize = fontSize;

		LoadFont();
	}

	Font::~Font()
	{
		Cleanup();
	}

	Font::Font(const Font& other)
		:m_pFont{ other.m_pFont },
		m_pFontTexture{ nullptr },
		m_Color{ other.m_Color },
		m_FontDesc{ other.m_FontDesc },
		m_Text{ other.m_Text }
	{
		FontManager::AddReference(m_pFont);
		LoadFontTexture();
	}

	Font::Font(Font&& other) noexcept
		:m_pFont{ other.m_pFont },
		m_pFontTexture{ other.m_pFontTexture },
		m_Color{other.m_Color},
		m_FontDesc{other.m_FontDesc},
		m_Text{other.m_Text}
	{
		other.m_pFont = nullptr;
		other.m_pFontTexture = nullptr;
	}

	Font& Font::operator=(const Font& other)
	{
		Cleanup();

		m_pFont = other.m_pFont;
		m_pFontTexture = nullptr;
		m_Color = other.m_Color;
		m_FontDesc = other.m_FontDesc;
		m_Text = other.m_Text;
		LoadFontTexture();

		return *this;
	}

	Font& Font::operator=(Font&& other) noexcept
	{
		Cleanup();

		m_pFont = other.m_pFont;
		m_pFontTexture = other.m_pFontTexture;
		m_Color = other.m_Color;
		m_FontDesc = other.m_FontDesc;
		m_Text = other.m_Text;

		other.m_pFont = nullptr;
		other.m_pFontTexture = nullptr;

		return *this;
	}

	void Font::SetText(const std::string& text)
	{
		m_Text = text;
		LoadFontTexture();
	}

	void Font::SetColor(const glm::vec4& color)
	{
		m_Color = color;
	}

	void Font::SetSize(size_t fontSize)
	{
		m_FontDesc.m_FontSize = fontSize;
		LoadFont();
	}

	void Font::SetFontPath(const std::string& path)
	{
		m_FontDesc.m_Path = path;
		LoadFont();
	}

	void Font::LoadFont()
	{
		if (m_pFont)
		{
			FontManager::ReleaseFont(m_pFont);
			m_pFont = nullptr;
		}

		m_pFont = FontManager::GetFont(&m_FontDesc);
		LoadFontTexture();
	}

	void Font::LoadFontTexture()
	{
		if (m_pFontTexture)
		{
			delete m_pFontTexture;
			m_pFontTexture = nullptr;
		}

		if(m_pFont)
			m_pFontTexture = m_pFont->GetFontTexture(m_Text);
	}

	void Font::Cleanup()
	{
		if (m_pFont)
		{
			FontManager::ReleaseFont(m_pFont);
			m_pFont = nullptr;
		}

		if (m_pFontTexture)
		{
			delete m_pFontTexture;
			m_pFontTexture = nullptr;
		}
	}

	void Font::Render(float x, float y)
	{
		if (m_pFontTexture)
		{
			m_pFontTexture->SetColor(m_Color);
			m_pFontTexture->Render(x, y);
		}
	}

	void Font::RenderImmediate(float x, float y)
	{
		if (m_pFontTexture)
		{
			m_pFontTexture->SetColor(m_Color);
			m_pFontTexture->RenderImmediate(x, y);
		}
	}

}
