#include "pch.h"
#include "Sound.h"

#include "Internal/ISound.h"
#include "Internal/Descriptors/SoundDescriptor.h"
#include "Internal/ResourceManagers/SoundManager.h"

no::Sound::Sound(const std::string& path)
	:m_pSound{ nullptr }
{
	SoundDescriptor soundDesc;
	soundDesc.m_Path = path;

	m_pSound = SoundManager::GetSound(&soundDesc);
}

no::Sound::~Sound()
{
	Cleanup();
}

no::Sound::Sound(const Sound& other)
	:m_pSound{ other.m_pSound }
{
	SoundManager::AddReference(m_pSound);
}

no::Sound::Sound(Sound&& other) noexcept
	: m_pSound{ other.m_pSound },
	m_ID{ other.m_ID }
{
	other.m_pSound = nullptr;
	other.m_ID = -1;
}

no::Sound& no::Sound::operator=(const Sound& other)
{
	Cleanup();

	m_pSound = SoundManager::AddReference(other.m_pSound);

	return *this;
}

no::Sound& no::Sound::operator=(Sound&& other) noexcept
{
	Cleanup();

	m_pSound = other.m_pSound;
	m_ID = other.m_ID;

	other.m_pSound = nullptr;
	other.m_ID = -1;

	return *this;
}

void no::Sound::Play()
{
	if (m_pSound)
		m_ID = m_pSound->Play();
}

void no::Sound::Play(int loops)
{
	if (m_pSound)
		m_ID = m_pSound->Play(loops);
}

void no::Sound::Pause()
{
	if (m_pSound && m_ID != -1)
		m_pSound->Pause(m_ID);
}

void no::Sound::Resume()
{
	if (m_pSound && m_ID != -1)
		m_pSound->Resume(m_ID);
}

void no::Sound::Stop()
{
	if (m_pSound && m_ID != -1)
	{
		m_pSound->Stop(m_ID);
		m_ID = -1;
	}
}

void no::Sound::Cleanup()
{
	Stop();
	if (m_pSound)
	{

		SoundManager::ReleaseSound(m_pSound);
	}
}
