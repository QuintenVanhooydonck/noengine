#pragma once
#include "NoEngine/Math/Vectors.h"

namespace no
{
	class ITexture;
	
	class Texture
	{
	public:
		Texture(const std::string& path);
		~Texture();

		Texture(const Texture& other);
		Texture(Texture&& other) noexcept;
		Texture& operator=(const Texture& other);
		Texture& operator=(Texture&& other) noexcept;

		void Render(float x, float y);

		void RenderImmediate(float x, float y);

		// call every frame just before render to set color modulation
		void SetColor(const glm::vec4& color);

	private:
		ITexture* m_pTexture;

		void Cleanup();
	};
}