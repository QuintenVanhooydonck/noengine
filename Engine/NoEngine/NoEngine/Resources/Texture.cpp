#include "pch.h"
#include "Texture.h"

#include "Internal/ITexture.h"
#include "Internal/Descriptors/TextureDescriptor.h"
#include "Internal/ResourceManagers/TextureManager.h"

namespace no
{
	Texture::Texture(const std::string& path)
		:m_pTexture{ nullptr }
	{
		TextureDescriptor textureDesc;
		textureDesc.m_Path = path;

		m_pTexture = TextureManager::GetTexture(&textureDesc);
	}
	
	Texture::~Texture()
	{
		Cleanup();
	}

	Texture::Texture(const Texture& other)
		:m_pTexture{ other.m_pTexture }
	{
		TextureManager::AddReference(m_pTexture);
	}

	Texture::Texture(Texture&& other) noexcept
		: m_pTexture{ other.m_pTexture }
	{
		other.m_pTexture = nullptr;
	}

	Texture& Texture::operator=(const Texture& other)
	{
		Cleanup();

		m_pTexture = TextureManager::AddReference(other.m_pTexture);

		return *this;
	}

	Texture& Texture::operator=(Texture&& other) noexcept
	{
		Cleanup();

		m_pTexture = other.m_pTexture;
		other.m_pTexture = nullptr;

		return *this;
	}

	void Texture::Render(float x, float y)
	{
		if(m_pTexture)
			m_pTexture->Render(x, y);
	}

	void Texture::RenderImmediate(float x, float y)
	{
		if (m_pTexture)
			m_pTexture->RenderImmediate(x, y);
	}

	void Texture::SetColor(const glm::vec4& color)
	{
		if (m_pTexture)
			m_pTexture->SetColor(color);
	}

	void Texture::Cleanup()
	{
		if (m_pTexture)
		{
			TextureManager::GetService()->ReleaseResource(m_pTexture);
			m_pTexture = nullptr;
		}
	}

}
