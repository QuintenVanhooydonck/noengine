#include "pch.h"
#include "HashString.h"

std::hash<std::string> HashString::m_Hasher;

HashString::HashString(const std::string& text)
	:m_Text{ text },
	m_Hash{ m_Hasher(text) }
{
}

HashString& HashString::operator=(const std::string& text)
{
	m_Text = text;
	m_Hash = m_Hasher(m_Text);

	return *this;
}

HashString& HashString::operator=(std::string&& text)
{
	m_Text = std::move(text);
	m_Hash = m_Hasher(m_Text);

	return *this;
}

const std::string& HashString::GetString() const
{
	return m_Text;
}

const size_t& HashString::GetHash() const
{
	return m_Hash;
}
