#include "pch.h"
#include "Music.h"
#include "Internal/Descriptors/MusicDescriptor.h"
#include "Internal/ResourceManagers/MusicManager.h"

no::Music::Music(const std::string& path)
	:m_pMusic{ nullptr }
{
	MusicDescriptor musicDesc;
	musicDesc.m_Path = path;

	m_pMusic = MusicManager::GetMusic(&musicDesc);
}

no::Music::~Music()
{
	Cleanup();
}

no::Music::Music(const Music& other)
	:m_pMusic{ other.m_pMusic }
{
	MusicManager::AddReference(other.m_pMusic);
}

no::Music::Music(Music&& other) noexcept
	: m_pMusic{ other.m_pMusic }
{
	other.m_pMusic = nullptr;
}

no::Music& no::Music::operator=(const Music& other)
{
	Cleanup();

	m_pMusic = MusicManager::AddReference(other.m_pMusic);

	return *this;
}

no::Music& no::Music::operator=(Music&& other) noexcept
{
	Cleanup();

	m_pMusic = other.m_pMusic;

	other.m_pMusic = nullptr;

	return *this;
}

void no::Music::Play()
{
	if (m_pMusic)
		m_pMusic->Play();
}

void no::Music::Pause()
{
	if (m_pMusic)
		m_pMusic->Pause();
}

void no::Music::Resume()
{
	if (m_pMusic)
		m_pMusic->Resume();
}

void no::Music::Stop()
{
	if (m_pMusic)
		m_pMusic->Stop();
}

void no::Music::Cleanup()
{
	if (m_pMusic)
	{
		MusicManager::ReleaseMusic(m_pMusic);
	}
}
