#pragma once

namespace no
{
	class ISound;

	class Sound
	{
	public:
		Sound(const std::string& path);
		~Sound();

		Sound(const Sound& other);
		Sound(Sound&& other) noexcept;
		Sound& operator=(const Sound& other);
		Sound& operator=(Sound&& other)noexcept;

		void Play();
		void Play(int loops);
		void Pause();
		void Resume();
		void Stop();

	private:
		ISound* m_pSound;
		int m_ID = -1;

		void Cleanup();
	};
}
