#include "pch.h"
#include "FPSComponent.h"
#include "TextComponent.h"
#include "NoEngine/Scene/GameObject.h"
#include "NoEngine/General/Time.h"

void no::FPSComponent::Start()
{
	GetGameObject()->RequireComponent<TextComponent>();
	m_TextComponent = GetComponent<TextComponent>();
}

void no::FPSComponent::Update()
{
	m_UpdateTime += Time::DeltaTime();
	++m_NumFrames;
	if(m_UpdateTime > m_TimeBetweenUpdates)
	{
		std::string text{ std::to_string(int(m_NumFrames / m_UpdateTime)) + " FPS" };
		m_TextComponent->SetText(text);

		m_UpdateTime = 0;
		m_NumFrames = 0;
	}
}
