#pragma once
#include "BaseComponent.h"

#include "NoEngine/Resources/Font.h"
#include "NoEngine/Math/Vectors.h"

namespace no
{
	class TextComponent : public BaseComponent
	{
	public:
		~TextComponent();

		virtual void Render() override;

		void Initialize(const std::string& path, const std::string& text, size_t fontSize = 20, const glm::vec4& color = { 1, 1, 1, 1 });

		void SetText(const std::string& text);
		void SetColor(const glm::vec4& color);

	private:
		Font* m_pFont = {};
		glm::vec4 m_Color;
	};
}