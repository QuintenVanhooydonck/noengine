#include "pch.h"
#include "TransformComponent.h"

const no::Transform& no::TransformComponent::GetTransform() const
{
	return m_Transform;
}

const glm::vec3& no::TransformComponent::GetPosition() const
{
	return GetTransform().GetPosition();
}

void no::TransformComponent::SetPosition(const glm::vec3& position)
{
	m_Transform.SetPosition(position.x, position.y, position.z);
}

void no::TransformComponent::SetPosition(float x, float y, float z)
{
	m_Transform.SetPosition(x, y, z);
}
