#pragma once
#include "NoEngine/Memory/PointerReference.h"
#include "NoEngine/Memory/ReferenceType.h"
#include "NoEngine/Scene/GameObject.h"

namespace no
{
	class GameObject;

	class BaseComponent : public ReferenceType<BaseComponent>
	{
		friend class GameObject;
	public:
		virtual ~BaseComponent() = 0 {};

		void Destroy() { m_IsDestroyed = true; }
		void Destroy(BaseComponent* component);
		void Destroy(GameObject* gameObject);

		GameObject* GetGameObject() const { return m_GameObject; }

	protected:
		BaseComponent() = default;

		// gets called when the object is created
		// variables are initialized here
		virtual void Initialize() {};
		// gets called before first frame, other objects are also initialized at this point
		virtual void Start() {};
		// gets called just before destructor gets called
		virtual void OnDestroy() {}
		// gets called every frame
		virtual void Update() {}
		// gets called at the end of every frame
		virtual void LateUpdate() {}
		// gets called at a fixed interval (can be called multiple times in 1 frame)
		virtual void FixedUpdate() {}
		// gets called when graphics should be rendered
		virtual void Render() {}

		template <typename T>
		T* GetComponent()
		{
			return GetGameObject()->GetComponent<T>();
		}

	private:
		GameObject* m_GameObject;
		bool m_IsDestroyed{ false }, m_Started{ false };

		void SetGameObject(GameObject* gameObject) { m_GameObject = gameObject; }
		void RootInitialize();
		void RootStart();
		void RootOnDestroy();
		void RootUpdate();
		void RootLateUpdate();
		void RootFixedUpdate();
		void RootRender();
	};
}