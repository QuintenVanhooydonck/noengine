#pragma once
#include "BaseComponent.h"
#include "NoEngine/Resources/Texture.h"
#include "NoEngine/Math/Vectors.h"


namespace no
{
	class TransformComponent;

	class RenderComponent : public BaseComponent
	{
	public:
		~RenderComponent();

		virtual void Start() override;
		virtual void Render() override;

		void SetTexture(const std::string& filename);
		void SetColor(const glm::vec4& color);

	private:
		PointerReference<TransformComponent> m_Transform;
		Texture* m_pRenderTexture{nullptr};
		glm::vec4 m_Color{ 1, 1, 1, 1 };
	};
}