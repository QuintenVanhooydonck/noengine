#pragma once
#include "BaseComponent.h"
#include "TextComponent.h"

namespace no
{
	class FPSComponent : public BaseComponent
	{
	public:
		virtual void Start() override;

		virtual void Update() override;

	private:
		PointerReference<TextComponent> m_TextComponent{};
		const float m_TimeBetweenUpdates{ 0.5f };
		float m_UpdateTime{ 0 };
		int m_NumFrames{ 0 };

	};
}