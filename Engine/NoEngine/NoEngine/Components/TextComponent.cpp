#include "pch.h"
#include "TextComponent.h"
#include "NoEngine/Scene/GameObject.h"

no::TextComponent::~TextComponent()
{
	if (m_pFont)
	{
		delete m_pFont;
		m_pFont = nullptr;
	}
}

void no::TextComponent::Render()
{
	m_pFont->SetColor(m_Color);
	m_pFont->Render(200, 200);
}

void no::TextComponent::Initialize(const std::string& path, const std::string& text, size_t fontSize, const glm::vec4& color)
{
	if (m_pFont)
	{
		delete m_pFont;
		m_pFont = nullptr;
	}

	m_pFont = new Font(path, text, fontSize);
	m_Color = color;
}

void no::TextComponent::SetText(const std::string& text)
{
	m_pFont->SetText(text);
}

void no::TextComponent::SetColor(const glm::vec4& color)
{
	m_Color = color;
}
