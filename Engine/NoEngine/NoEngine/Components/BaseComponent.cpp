#include "pch.h"
#include "BaseComponent.h"

void no::BaseComponent::Destroy(BaseComponent* component)
{
	if(component)
		component->Destroy();
}

void no::BaseComponent::Destroy(GameObject* gameObject)
{
	if (gameObject)
		gameObject->Destroy();
}

void no::BaseComponent::RootInitialize()
{
	Initialize();
}

void no::BaseComponent::RootStart()
{
	Start();
}

void no::BaseComponent::RootOnDestroy()
{
	OnDestroy();
}

void no::BaseComponent::RootUpdate()
{
	Update();
}

void no::BaseComponent::RootLateUpdate()
{
	LateUpdate();
}

void no::BaseComponent::RootFixedUpdate()
{
	FixedUpdate();
}

void no::BaseComponent::RootRender()
{
	Render();
}
