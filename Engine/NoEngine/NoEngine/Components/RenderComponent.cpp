#include "pch.h"
#include "RenderComponent.h"
#include "TransformComponent.h"
#include "NoEngine/Scene/GameObject.h"

no::RenderComponent::~RenderComponent()
{
	if (m_pRenderTexture)
	{
		delete m_pRenderTexture;
		m_pRenderTexture = nullptr;
	}
}

void no::RenderComponent::Start()
{
	GetGameObject()->RequireComponent<TransformComponent>();
	m_Transform = GetComponent<TransformComponent>();
}

void no::RenderComponent::Render()
{
	if (!m_pRenderTexture)
		return;

	auto pos = m_Transform->GetPosition();

	m_pRenderTexture->SetColor(m_Color);
	m_pRenderTexture->Render(pos.x, pos.y);
}

void no::RenderComponent::SetTexture(const std::string& filename)
{
	if (m_pRenderTexture)
	{
		delete m_pRenderTexture;
		m_pRenderTexture = nullptr;
	}

	m_pRenderTexture = new Texture(filename);
}

void no::RenderComponent::SetColor(const glm::vec4& color)
{
	m_Color = color;
}
