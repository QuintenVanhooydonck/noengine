#pragma once
#include "BaseComponent.h"
#include "NoEngine/Math/Transform.h"

namespace no
{
	class TransformComponent : public BaseComponent
	{
	public:
		const Transform& GetTransform() const;
		const glm::vec3& GetPosition() const;
		void SetPosition(const glm::vec3& position);
		void SetPosition(float x, float y, float z = 0.0f);

	private:
		Transform m_Transform{};
	};
}
