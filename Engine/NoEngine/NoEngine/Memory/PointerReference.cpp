#include "pch.h"
#include "PointerReference.h"
std::vector<std::pair<no::GenericPointerRef*, int>> no::GenericPointerRef::m_Refs{};

void no::GenericPointerRef::Load(int id)
{
	m_Refs.push_back({ this, id });
}

void no::GenericPointerRef::Initialize()
{
	for (auto ref : m_Refs)
		ref.first->SetReference(ref.second);
	for (auto ref : m_Refs)
		ref.first->EraseReferences();
	m_Refs.clear();
}