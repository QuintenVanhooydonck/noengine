#pragma once
#include <utility>
#include <vector>
#include "ReferenceType.h"

namespace no
{
	// Is used to assign a referenceType to a variable, can be used to check if the object is still alive
	// or can be used for loading and saving
	class GenericPointerRef
	{
	public:
		void Load(int id);

		static void Initialize();
	protected:
		virtual void SetReference(int id) = 0;
		virtual void EraseReferences() = 0;

	private:
		static std::vector<std::pair<GenericPointerRef*, int>> m_Refs;
	};

	template<typename T>
	class PointerReference final : public GenericPointerRef
	{
	public:
		PointerReference(T* ref = nullptr);
		~PointerReference() = default;

		PointerReference& operator=(T* ref);

		//rule of 5
		PointerReference(const PointerReference& ref);
		PointerReference(PointerReference&& ref);
		PointerReference& operator=(const PointerReference& ref);
		PointerReference& operator=(PointerReference&& ref);
		
		friend bool operator==(const PointerReference<T>& ref, T* ptr) { return ref.m_Reference == ptr; }
		friend bool operator==(T* ptr, const PointerReference<T>& ref) { return ref.m_Reference == ptr; }
		friend bool operator!=(const PointerReference<T>& ref, T* ptr) { return ref.m_Reference != ptr; }
		friend bool operator!=(T* ptr, const PointerReference<T>& ref) { return ref.m_Reference != ptr; }

		operator bool() const;
		T* operator->() const;
		operator T* () const;

		int GetId() const;
	protected:
		virtual void SetReference(int id) override;
		virtual void EraseReferences() override;
	private:
		T* m_Reference{};
	};

	template<typename T>
	inline PointerReference<T>::PointerReference(T* ref)
		:m_Reference{ ref }
	{
	}

	template<typename T>
	inline PointerReference<T>& PointerReference<T>::operator=(T* ref)
	{
		m_Reference = ref;
		return *this;
	}

	template<typename T>
	inline PointerReference<T>::PointerReference(const PointerReference& ref)
		:m_Reference{ ref.m_Reference }
	{
	}

	template<typename T>
	inline PointerReference<T>::PointerReference(PointerReference&& ref)
		: m_Reference{ std::exchange(ref.m_Reference, nullptr) }
	{
	}

	template<typename T>
	inline PointerReference<T>& PointerReference<T>::operator=(const PointerReference& ref)
	{
		m_Reference = ref.m_Reference;
		return *this;
	}

	template<typename T>
	inline PointerReference<T>& PointerReference<T>::operator=(PointerReference&& ref)
	{
		m_Reference = std::exchange(ref.m_Reference, nullptr);
		return *this;
	}

	template<typename T>
	inline PointerReference<T>::operator bool() const
	{
		return T::IsReferenceAlive(m_Reference);
	}

	template<typename T>
	inline T* PointerReference<T>::operator->() const
	{
		return m_Reference;
	}

	template<typename T>
	inline PointerReference<T>::operator T* () const
	{
		if (this)
			return m_Reference;
		else
			return nullptr;
	}

	template<typename T>
	inline int PointerReference<T>::GetId() const
	{
		if (m_Reference)
			return m_Reference->GetIdentifier();
		else
			return -1;
	}

	template<typename T>
	inline void PointerReference<T>::SetReference(int id)
	{
		m_Reference = dynamic_cast<T*>(T::GetReference(id)); // dynamic cast as this only happens in the loading stage
	}

	template<typename T>
	inline void PointerReference<T>::EraseReferences()
	{
		ReferenceType<T>::EraseAllIdentifiers();
	}
}