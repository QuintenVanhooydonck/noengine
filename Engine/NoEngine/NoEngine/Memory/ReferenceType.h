#pragma once
#include <unordered_set>
#include <unordered_map>
#include <random>

namespace no
{
	// contains the reference id and function to generate new id's
	struct ReferenceIdentifier
	{
		int id;

	public:
		ReferenceIdentifier() :id{ GetId() } {}

		static void PushLoadPrefab() { m_PrefabIDOffset = GetId(); }
		static void PopLoadPrefab() { m_PrefabIDOffset = 0; }
		static int GetIDOffset() { return m_PrefabIDOffset; };

	private:
		static int GetId() {
			static std::uniform_int_distribution<int> distribution(INT_MIN, INT_MAX);
			return distribution(m_Generator); // better distribution than std::rand 
		};
		static std::default_random_engine m_Generator;
		static int m_PrefabIDOffset;
	};

	// assigns an id to derived classes so they can be found later on and be checked if it is still alive
	template <typename T>
	class ReferenceType
	{
	public:
		ReferenceType() { AddReference(this); };
		virtual ~ReferenceType() { RemoveReference(this); };
		// if reference is in set, it's alive
		static bool IsReferenceAlive(ReferenceType* reference);
		static T* GetReference(int id);
		int GetIdentifier() const { return m_Identifier.id; }
		void SetIdentifier(int id);
		static void EraseAllIdentifiers();

	private:
		static std::unordered_set<ReferenceType*> m_RefObjects;
		static std::unordered_map<int, ReferenceType*> m_IdToRef;
		ReferenceIdentifier m_Identifier{};

		//add reference to reference set
		void AddReference(ReferenceType* reference);
		//remove reference from reference set
		void RemoveReference(ReferenceType* reference);
	};

	template <typename T>
	std::unordered_set<ReferenceType<T>*> ReferenceType<T>::m_RefObjects{};
	template <typename T>
	std::unordered_map<int, ReferenceType<T>*> ReferenceType<T>::m_IdToRef{};


	template <typename T>
	void ReferenceType<T>::AddReference(ReferenceType<T>* reference)
	{
		m_RefObjects.insert(reference);
		m_IdToRef[m_Identifier.id] = reference;
	}

	template <typename T>
	void ReferenceType<T>::RemoveReference(ReferenceType<T>* reference)
	{
		m_RefObjects.erase(reference);
		m_IdToRef.erase(m_Identifier.id);
	}

	template <typename T>
	bool ReferenceType<T>::IsReferenceAlive(ReferenceType<T>* reference)
	{
		return m_RefObjects.count(reference);
	}

	template<typename T>
	inline T* ReferenceType<T>::GetReference(int id)
	{
		if (m_IdToRef.count(id))
			return static_cast<T*>(m_IdToRef[id]);
		return nullptr;
	}

	template<typename T>
	inline void ReferenceType<T>::SetIdentifier(int id)
	{
		// this function overrides the old identifier to the new one
		// set by the parameter id

		// if id already is added to the map, remove it
		if (!m_IdToRef.count(m_Identifier.id))
		{
			m_IdToRef.erase(m_Identifier.id);
		}

		// set the current identifier id to the file id
		// and store this in the map again
		m_Identifier.id = id;
		m_IdToRef[m_Identifier.id] = this;
	}

	template<typename T>
	inline void ReferenceType<T>::EraseAllIdentifiers()
	{
		m_IdToRef.clear();
	}
}