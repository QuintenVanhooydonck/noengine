#pragma once
#include <functional>
#include <chrono>
#include <thread>
#include <mutex>

namespace no
{
	class PeriodicTask final
	{
	public:
		// time in seconds before repeated
		// function to excecute every repeated time
		PeriodicTask(float repeatTime, const std::function<void(void)>& task);
		~PeriodicTask();

		PeriodicTask(const PeriodicTask& other) = delete;
		PeriodicTask(PeriodicTask&& other) = default;
		PeriodicTask& operator=(const PeriodicTask& other) = delete;
		PeriodicTask& operator=(PeriodicTask&& other) = default;

	private:
		std::thread m_Thread{};
		std::condition_variable m_CV{};
		bool m_Stop = false;

		// is not a reference as the reference is broken when this function runs
		void AsyncTask(std::chrono::milliseconds delay, std::function<void(void)> task);
	};
}