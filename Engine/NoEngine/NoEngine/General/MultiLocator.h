#pragma once
#include <forward_list>

namespace no
{
	// -----------------------------------------------
	// declaration
	// -----------------------------------------------
	template <typename T>
	class MultiLocator
	{
	public:
		~MultiLocator();

		static std::forward_list<T*> GetServices() { return m_pServices; }

		static size_t Provide(T* pService);

		static void Destroy(size_t id);
		// removes the item from the locator without destroying it
		static void Remove(size_t id);

	private:
		static std::forward_list<T*> m_pServices;
	};


	// -----------------------------------------------
	// definition
	// -----------------------------------------------
	template <typename T>
	std::forward_list<T*> MultiLocator<T>::m_pServices;

	template <typename T>
	MultiLocator<T>::~MultiLocator()
	{
		Destroy();
	}

	template <typename T>
	size_t MultiLocator<T>::Provide(T* pService)
	{
		m_pServices.push_front(pService);
		return (size_t)pService;
	}

	template <typename T>
	void MultiLocator<T>::Destroy(size_t id)
	{
		T* service = (T*)id;

		if (!std::count(m_pServices.cbegin(), m_pServices.cend(), service))
			return;

		m_pServices.remove(service);
		delete service;
	}
	template<typename T>
	inline void MultiLocator<T>::Remove(size_t id)
	{
		T* service = (T*)id;

		if (!std::count(m_pServices.cbegin(), m_pServices.cend(), service))
			return;

		m_pServices.remove(service);
	}
}