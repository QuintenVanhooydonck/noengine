#pragma once
#include "Singleton.h"

namespace no
{
	class Time final : public no::Singleton<Time>
	{
		friend class Core;

	public:
		static float DeltaTime();
		static float FixedDeltaTime();
		static float TotalTime();

		static std::string GetTimeStamp();
	private:
		float m_DeltaTime{};
		float m_FixedDeltaTime{};
		float m_TotalTime{ 0 };

		float InternalDeltaTime() const;
		float InternalFixedDeltaTime() const;
		float InternalTotalTime() const;

		static void SetDeltaTime(float deltaTime);
		void InternalSetDeltaTime(float deltaTime);

		static void SetFixedDeltatTime(float fixedDeltaTime);
		void InternalSetFixedDeltaTime(float fixedDeltaTime);

		static void AddTotalTime(float deltaTime);
		void InternalAddTotalTime(float deltaTime);

		static void ResetTime();
		void InternalResetTime();
	};
}
