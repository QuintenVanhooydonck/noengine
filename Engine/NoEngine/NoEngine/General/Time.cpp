#include "pch.h"
#include "Time.h"
#include <time.h>

float no::Time::DeltaTime()
{
	return GetInstance().InternalDeltaTime();
}

float no::Time::FixedDeltaTime()
{
	return GetInstance().InternalFixedDeltaTime();
}

float no::Time::TotalTime()
{
	return GetInstance().InternalTotalTime();
}

std::string no::Time::GetTimeStamp()
{
	time_t timeDate = time(0);
	tm localTime;
	localtime_s(&localTime, &timeDate);

	char buffer[3]{}; // add ending 0 for print

	std::string timeStamp{};
	//timeStamp += 1900 + localTime->tm_year + ' ';
	std::snprintf(buffer, 3, "%02d", localTime.tm_mon + 1);
	timeStamp += std::string(buffer) + '/';
	std::snprintf(buffer, 3, "%02d", localTime.tm_mday);
	timeStamp += std::string(buffer) + ' ';

	std::snprintf(buffer, 3, "%02d", localTime.tm_hour);
	timeStamp += std::string(buffer) + ':';
	std::snprintf(buffer, 3, "%02d", localTime.tm_min);
	timeStamp += std::string(buffer) + ':';
	std::snprintf(buffer, 3, "%02d", localTime.tm_sec);
	timeStamp += std::string(buffer);

	return timeStamp;
}

float no::Time::InternalDeltaTime() const
{
	return m_DeltaTime;
}

float no::Time::InternalFixedDeltaTime() const
{
	return m_FixedDeltaTime;
}

float no::Time::InternalTotalTime() const
{
	return m_TotalTime;
}

void no::Time::SetDeltaTime(float deltaTime)
{
	GetInstance().InternalAddTotalTime(deltaTime);
	GetInstance().InternalSetDeltaTime(deltaTime);
}

void no::Time::InternalSetDeltaTime(float deltaTime)
{
	m_DeltaTime = deltaTime;
}

void no::Time::SetFixedDeltatTime(float fixedDeltaTime)
{
	GetInstance().InternalSetFixedDeltaTime(fixedDeltaTime);
}

void no::Time::InternalSetFixedDeltaTime(float fixedDeltaTime)
{
	m_FixedDeltaTime = fixedDeltaTime;
}

void no::Time::AddTotalTime(float deltaTime)
{
	GetInstance().InternalAddTotalTime(deltaTime);
}

void no::Time::InternalAddTotalTime(float deltaTime)
{
	m_TotalTime += deltaTime;
}

void no::Time::ResetTime()
{
	GetInstance().InternalResetTime();
}

void no::Time::InternalResetTime()
{
	m_DeltaTime = 0;
	m_TotalTime = 0;
}
