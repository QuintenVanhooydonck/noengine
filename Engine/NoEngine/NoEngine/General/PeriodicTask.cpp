#include "pch.h"
#include "PeriodicTask.h"

no::PeriodicTask::PeriodicTask(float repeatTime, const std::function<void(void)>& task)
{
	m_Thread = std::thread(&no::PeriodicTask::AsyncTask, this, std::chrono::milliseconds(int(repeatTime * 1000)), task);
}

no::PeriodicTask::~PeriodicTask()
{
	m_Stop = true;
	m_CV.notify_all();
	m_Thread.join();
}

void no::PeriodicTask::AsyncTask(std::chrono::milliseconds delay, std::function<void(void)> task)
{
	std::mutex mut{};
	std::unique_lock<std::mutex> lock(mut);
	while (true)
	{
		if (m_CV.wait_for(lock, std::chrono::milliseconds(delay), [this] {return m_Stop; }))
		{
			task();// flush file data just before ending logger
			return;
		}
		else
		{
			task();
		}
	}
}
