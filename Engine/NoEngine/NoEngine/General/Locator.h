#pragma once

namespace no
{
	// -----------------------------------------------
	// declaration
	// -----------------------------------------------
	template <typename T>
	class Locator
	{
	public:
		~Locator();

		static T* GetService() { return m_pService; }

		static void Provide(T* pService);

		static void Destroy();

	private:
		static T* m_pService;
	};


	// -----------------------------------------------
	// defenition
	// -----------------------------------------------
	template <typename T>
	T* Locator<T>::m_pService;

	template <typename T>
	Locator<T>::~Locator()
	{
		Destroy();
	}

	template <typename T>
	void Locator<T>::Provide(T* pService)
	{
		m_pService = pService;
	}

	template <typename T>
	void Locator<T>::Destroy()
	{
		if (m_pService)
		{
			delete m_pService;
			m_pService = nullptr;
		}
	}
}