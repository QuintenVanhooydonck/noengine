#include "Defines.h"
#if defined(TEST_INTERACTIVE) && defined(TEST_INPUT)
#include "catch.hpp"

#include <NoEngine/Core.h>
#include <NoEngine/Modules/Module.h>
#include <SDLEvent/ModuleSDLEvent.h>
#include <SDLImage/ModuleSDLImage.h>
#include <SFMLImage/ModuleSFMLImage.h>
#include <SFMLEvent/ModuleSFMLEvent.h>

#ifdef TEST_NO_MODULE

TEST_CASE("Test SDL Input")
{
	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSDLImage(500, 500, "Hello"));
	modules.push_back(new no::ModuleSDLEvent());

	no::Core engine{ modules };
	engine.Initialize();
	engine.Run();
}
#endif // TEST_NO_MODULE

#ifdef TEST_SFML

TEST_CASE("Test SFML Input")
{
	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSFMLImage(500, 500, "Hello"));
	modules.push_back(new no::ModuleSFMLEvent());
	no::Core engine{ modules };
	engine.Initialize();

	engine.Cleanup();
}
#endif // TEST_SFML
#endif