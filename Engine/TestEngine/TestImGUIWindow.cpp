#include "Defines.h"
#if defined(TEST_INTERACTIVE) && defined(TEST_IMGUI)
#include "catch.hpp"

#include <NoEngine/Core.h>

#include <SDLImage/ModuleSDLImage.h>
#include <SDLEvent/ModuleSDLEvent.h>
#include <ModuleEditorSDL/ModuleEditorSDL.h>
#include <NoEngine/Render/Renderer.h>
#include "NoEngine/Render/Internal/IWindow.h"
#include "NoEngine/Render/Window.h"
#include "NoEngine/Scene/SceneManager.h"
#include "NoEngine/Scene/GameObject.h"
#include "NoEngine/Components/TransformComponent.h"
#include "NoEngine/Components/TextComponent.h"
#include "NoEngine/Components/FPSComponent.h"
#include "NoEngine/Scene/Scene.h"
#include "SFMLImage/ModuleSFMLImage.h"
#include "SFMLEvent/ModuleSFMLEvent.h"
#include "ModuleEditorSFML/ModuleEditorSFML.h"
#include "NoEngine/Modules/Logger/ModuleLogConsole/ModuleLogConsole.h"

void AddComponents()
{
	no::SceneManager::GetInstance().CreateScene("Test");

	auto test = no::GameObject::Create();
	auto trans = test->AddComponent<no::TransformComponent>();
	trans->SetPosition({ 600, 600, 0 });
	auto textComponent = test->AddComponent<no::TextComponent>();
	textComponent->Initialize("Lingua.otf", "Hello World!");
	test->AddComponent<no::FPSComponent>();
	//no::SceneManager::GetInstance().GetScenes()[0]->Add(test);
}
#ifdef TEST_SDL
TEST_CASE("Test imgui window SDL")
{
	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSDLImage(640, 480, "Test SDL Image module"));
	modules.push_back(new no::ModuleSDLEvent());
	modules.push_back(new noEditor::ModuleEditorSDL());

	no::Window::GetService()->SetWindowResizable(true);

	no::Core engine{ modules, "../Data/" };
	engine.Initialize();

	AddComponents();

	while (!no::Core::IsQuitting())
	{
		engine.UpdateModules();
		no::Renderer::StartRender();
		engine.RenderModules();
		no::Renderer::EndRender();
	}
}
#endif

#ifdef TEST_SFML
TEST_CASE("Test imgui window SFML")
{
	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSFMLImage(640, 480, "Test SDL Image module"));
	modules.push_back(new no::ModuleSFMLEvent());
	modules.push_back(new noEditor::ModuleEditorSFML());
	modules.push_back(new no::ModuleLogConsole());

	no::Window::GetService()->SetWindowResizable(true);

	no::Core engine{ modules, "../Data/" };
	engine.Initialize();

	AddComponents();

	while (!no::Core::IsQuitting())
	{
		engine.UpdateModules();
		no::Renderer::StartRender();
		engine.RenderModules();
		no::Renderer::EndRender();
	}

	engine.Cleanup();
}
#endif

#endif
