#pragma once
#define TEST_INTERACTIVE

#define TEST_IMAGE
#define TEST_AUDIO
#define TEST_INPUT
#define TEST_WINDOW
#define TEST_IMGUI

#define TEST_NO_MODULE
#define TEST_SDL
#define TEST_SFML