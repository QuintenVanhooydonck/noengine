#include "Defines.h"
#if defined(TEST_INTERACTIVE) && defined(TEST_WINDOW)
#include "catch.hpp"

#include <NoEngine/Modules/Module.h>
#include <SDLImage/ModuleSDLImage.h>
#include <SFMLImage/ModuleSFMLImage.h>
#include <SDLEvent/ModuleSDLEvent.h>
#include <SFMLEvent/ModuleSFMLEvent.h>
#include <NoEngine/Core.h>
#include <NoEngine/Scene/GameObject.h>
#include <NoEngine/Scene/Scene.h>
#include <NoEngine/Scene/SceneManager.h>
#include <NoEngine/Render/Window.h>
#include <NoEngine/Components/TextComponent.h>
#include <NoEngine/Components/FPSComponent.h>
#include <NoEngine/Components/TransformComponent.h>

#include "ApprovalText.h"

void WindowTests()
{
	REQUIRE(ApprovalText::Question("Did you see a separate window?"));

	no::Window::SetWindowSize(800, 600);
	REQUIRE(ApprovalText::Question("Did the screen become larger?"));

	no::Window::SetWindowTitle("Title Test");
	REQUIRE(ApprovalText::Question("Did the window title change?"));

	no::Window::SetWindowFullscreen(no::ScreenMode::borderlessFullscreen);
	REQUIRE(ApprovalText::Question("Did the window change to borderless?"));

	no::Window::SetWindowFullscreen(no::ScreenMode::fullscreen);
	REQUIRE(ApprovalText::Question("Did the window change to fullscreen?"));

	no::Window::SetWindowFullscreen(no::ScreenMode::windowed);
	REQUIRE(ApprovalText::Question("Did the window return windowed mode?"));
}

void AddComponentsWindow()
{
	no::SceneManager::GetInstance().CreateScene("Test");

	auto test = no::GameObject::Create();
	test->AddComponent<no::TransformComponent>();
	auto textComponent = test->AddComponent<no::TextComponent>();
	textComponent->Initialize("Lingua.otf", "Hello World!");
	test->AddComponent<no::FPSComponent>();
	//no::SceneManager::GetInstance().GetScenes()[0]->Add(test);
}

void TestVsync(no::Module* windowModule, no::Module* eventModule, bool on)
{
	std::vector<no::Module*> modules{};
	modules.push_back(windowModule);
	modules.push_back(eventModule);

	no::Core engine{ modules, "../Data/" };
	engine.Initialize();
	no::Window::SetVsync(on);

	std::cout << "Close the window when you're finished, read the fps in the window\n";

	AddComponentsWindow();
	engine.Run();
	engine.Cleanup();

	REQUIRE(ApprovalText::Question("Was VSync on?") == on);
}

#ifdef TEST_SDL


TEST_CASE("Test SDL Image Module Window")
{
	ApprovalText::Title("Test window SDL Module");

	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSDLImage(640, 480, "Test SDL Window module"));
	modules.push_back(new no::ModuleSDLEvent());

	no::Core engine{ modules, "../Data/" };

	engine.Initialize();

	WindowTests();

	engine.Cleanup();
}
#endif // TEST_SDL

#ifdef TEST_SFML

TEST_CASE("Test SFML Image Module Window")
{
	ApprovalText::Title("Test window SFML Module");

	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSFMLImage(640, 480, "Test SFML Window module"));
	modules.push_back(new no::ModuleSFMLEvent());

	no::Core engine{ modules, "../Data/" };

	engine.Initialize();

	WindowTests();
	engine.Cleanup();

	TestVsync(new no::ModuleSFMLImage(640, 480, "Test SFML vsync"), new no::ModuleSFMLEvent(), false);
	TestVsync(new no::ModuleSFMLImage(640, 480, "Test SFML vsync"), new no::ModuleSFMLEvent(), true);

}
#endif // TEST_SFML
#endif
