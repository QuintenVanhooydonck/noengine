#include "Defines.h"
#if defined(TEST_INTERACTIVE) && defined(TEST_AUDIO)

#include "catch.hpp"

#include <NoEngine/Core.h>
#include <NoEngine/Modules/Module.h>
#include <SDLAudio/ModuleSDLAudio.h>

#include <NoEngine/Resources/Sound.h>
#include <NoEngine/Resources/Music.h>

#include "ApprovalText.h"

void PlayAudio(bool hasModule = true)
{
	no::Sound test = no::Sound("TestWav.wav");

	test.Play();
	REQUIRE(ApprovalText::Question("Do you hear a sound play?") == hasModule);

	test.Play(10000);
	REQUIRE(ApprovalText::Question("Do you hear a sound play on loop?") == hasModule);

	test.Pause();
	REQUIRE(ApprovalText::Question("Did the sound stop?") == hasModule);
	
	test.Resume();
	REQUIRE(ApprovalText::Question("Did the sound resume?") == hasModule);

	test.Stop();
	REQUIRE(ApprovalText::Question("Did the sound stop?") == hasModule);


	no::Sound testNoSound = no::Sound("NoPath");
	testNoSound.Play();
}

void PlayMusic(bool hasModule = true)
{
	no::Music test = no::Music("audioTest.ogg");

	test.Play();
	REQUIRE(ApprovalText::Question("Do you hear music play?") == hasModule);

	test.Pause();
	REQUIRE(ApprovalText::Question("Did the music stop?") == hasModule);

	test.Resume();
	REQUIRE(ApprovalText::Question("Did the music resume?") == hasModule);

	test.Stop();
	REQUIRE(ApprovalText::Question("Did the music stop?") == hasModule);

	no::Music testNoMusic = no::Music("NoPath");
	testNoMusic.Play();
}

#ifdef TEST_NO_MODULE

TEST_CASE("Test No Audio Module")
{
	ApprovalText::Title("Test No Audio Module");
	std::vector<no::Module*> modules{};

	no::Core engine{ modules, "../Data/" };
	engine.Initialize();

	PlayAudio(false);
	PlayMusic(false);

	engine.Cleanup();
}
#endif // TEST_NO_MODULE

#ifdef TEST_SDL

TEST_CASE("Test SDL Audio Module")
{
	ApprovalText::Title("Test SDL Audio Module");

	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSDLAudio());

	no::Core engine{ modules, "../Data/" };
	engine.Initialize();

	PlayAudio();
	PlayMusic();

	engine.Cleanup();
}
#endif // TEST_SDL

#endif
