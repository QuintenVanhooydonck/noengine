#pragma once
#include <string>

namespace ApprovalText
{
	bool Question(const std::string& question);
	void Title(const std::string& title);
}