#include "catch.hpp"

#include <NoEngine/Components/BaseComponent.h>
#include <NoEngine/Scene/GameObject.h>
#include <NoEngine/Scene/SceneManager.h>
#include <NoEngine/Core.h>

class TestComponent : public no::BaseComponent
{
public:
	bool m_Initialized{};
	bool m_Started{};
	bool m_Updated{};
	bool m_LateUpdated{};
	bool* m_Destroyed{};

	void SetDestroyedVar(bool& isDestroyed) { m_Destroyed = &isDestroyed; }

protected:
	virtual void Initialize() override
	{
		m_Initialized = true;
	}

	virtual void Start() override
	{
		m_Started = true;
	}

	virtual void OnDestroy() override
	{
		*m_Destroyed = true;
	}

	virtual void Update() override
	{
		m_Updated = true;
	}

	virtual void LateUpdate() override
	{
		m_LateUpdated = true;
	}

};

TEST_CASE("Test components")
{
	no::Core engine = no::Core();
	engine.Initialize();
	no::SceneManager::GetInstance().CreateScene("Test");

	bool isDestroyed{ false };

	auto go = no::GameObject::Create();
	no::PointerReference<TestComponent> testComp = go->AddComponent<TestComponent>();
	testComp->SetDestroyedVar(isDestroyed);

	REQUIRE(testComp->GetGameObject());

	REQUIRE(testComp->m_Initialized);
	REQUIRE_FALSE(testComp->m_Started);
	REQUIRE_FALSE(testComp->m_Updated);
	REQUIRE_FALSE(testComp->m_LateUpdated);
	REQUIRE_FALSE(isDestroyed);

	engine.Step(0.02f);

	REQUIRE(testComp->m_Initialized);
	REQUIRE(testComp->m_Started);
	REQUIRE(testComp->m_Updated);
	REQUIRE(testComp->m_LateUpdated);
	REQUIRE_FALSE(isDestroyed);

	testComp->Destroy();
	engine.Step(0.02f);

	REQUIRE(isDestroyed);
	REQUIRE_FALSE(testComp);
}