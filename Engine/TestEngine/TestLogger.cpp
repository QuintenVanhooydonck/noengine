#pragma once
#include "catch.hpp"

#include <string>

#include <NoEngine/Core.h>
#include <NoEngine/Debug/Logger.h>
#include <NoEngine/Debug/Internal/ILogger.h>

class TestLogger : public no::ILogger
{
public:
	virtual void Log(const std::string& info, const std::string& file, const std::string& function, int line) override
	{
		m_LastMessage = info;
		m_LastFile = file;
		m_LastFunction = function;
		m_LastLine = line;
	}

	virtual void Warning(const std::string& warning, const std::string& file, const std::string& function, int line) override
	{
		m_LastMessage = warning;
		m_LastFile = file;
		m_LastFunction = function;
		m_LastLine = line;
	}

	virtual void Exception(const std::string& error, const std::string& file, const std::string& function, int line) override
	{
		m_LastMessage = error;
		m_LastFile = file;
		m_LastFunction = function;
		m_LastLine = line;
	}

public:
	std::string m_LastMessage{};
	std::string m_LastFile{};
	std::string m_LastFunction{};
	int m_LastLine{};
};

TEST_CASE("Test Logger")
{
	no::Core engine = no::Core();
	engine.Initialize();

	TestLogger logger = TestLogger();
	int line;
	auto id = no::Logger::Provide(&logger);

	no::Logger::SetLogTimeStamp(false);
	no::Logger::SetLogType(true);

	no::Logger::LOG_INFO("Test Info"); line = __LINE__;
	REQUIRE(logger.m_LastMessage == "[I] Test Info");
	REQUIRE(logger.m_LastFile == __FILE__);
	REQUIRE(logger.m_LastFunction == __FUNCTION__);
	REQUIRE(logger.m_LastLine == line);
	REQUIRE(logger.m_LastMessage == "[I] Test Info");
	REQUIRE(logger.m_LastFile == __FILE__);
	REQUIRE(logger.m_LastFunction == __FUNCTION__);
	REQUIRE(logger.m_LastLine == line);

	no::Logger::LOG_WARNING("Test Warning"); line = __LINE__;
	REQUIRE(logger.m_LastMessage == "[W] Test Warning");
	REQUIRE(logger.m_LastFile == __FILE__);
	REQUIRE(logger.m_LastFunction == __FUNCTION__);
	REQUIRE(logger.m_LastLine == line);

	no::Logger::LOG_ERROR("Test Error"); line = __LINE__;
	REQUIRE(logger.m_LastMessage == "[E] Test Error");
	REQUIRE(logger.m_LastFile == __FILE__);
	REQUIRE(logger.m_LastFunction == __FUNCTION__);
	REQUIRE(logger.m_LastLine == line);

	no::Logger::Remove(id);
}