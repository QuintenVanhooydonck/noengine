#include "catch.hpp"

#include <NoEngine/Core.h>
#include <NoEngine/General/Time.h>

TEST_CASE("Test Time")
{
	no::Core engine = no::Core();
	engine.Initialize();

	engine.Step(0.02f);
	REQUIRE(no::Time::DeltaTime() == 0.02f);
	REQUIRE(no::Time::TotalTime() == 0.02f);

	engine.Step(0.04f);
	REQUIRE(no::Time::DeltaTime() == 0.04f);
	REQUIRE(no::Time::TotalTime() == 0.06f);

	engine.Step(0.1f);
	REQUIRE(no::Time::DeltaTime() == 0.1f);
	REQUIRE(no::Time::TotalTime() == 0.16f);
}