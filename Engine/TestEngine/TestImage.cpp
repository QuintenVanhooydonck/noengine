#include "Defines.h"
#if defined(TEST_INTERACTIVE) && defined(TEST_IMAGE)
#include "catch.hpp"

#include <NoEngine/Core.h>
#include <SDLEvent/ModuleSDLEvent.h>
#include <SDLImage/ModuleSDLImage.h>
#include <SFMLImage/ModuleSFMLImage.h>
#include <SFMLEvent/ModuleSFMLEvent.h>
#include <NoEngine/Modules/Module.h>
#include <NoEngine/Resources/Texture.h>
#include <NoEngine/Resources/Font.h>
#include <NoEngine/Scene/SceneManager.h>
#include <NoEngine/Scene/GameObject.h>
#include <NoEngine/Scene/Scene.h>
#include <NoEngine/Render/Renderer.h>

#include <NoEngine/Components/RenderComponent.h>
#include <NoEngine/Components/TextComponent.h>
#include <NoEngine/Components/TransformComponent.h>

#include "ApprovalText.h"

void RenderImages(bool testTrue)
{
	no::Texture testTexture = no::Texture("Logo.png");
	{
		no::Texture testSameTexture = no::Texture("Logo.png");
	}
	no::Renderer::Clear();
	testTexture.RenderImmediate(0, 0);
	REQUIRE(ApprovalText::Question("Did you see an image?") == testTrue);

	no::Renderer::Clear();
	testTexture.SetColor({ 1, 0, 0, 1 });
	testTexture.RenderImmediate(0, 0);
	REQUIRE(ApprovalText::Question("Is the image red now?") == testTrue);

	no::Texture testNonExist = no::Texture("NoPath");
	testNonExist.RenderImmediate(50, 50);
}

void RenderFonts(bool testTrue)
{
	no::Font testFont = no::Font("Lingua.otf", "Hello World");
	{
		no::Font testSameFont = no::Font("Lingua.otf", "Hello World");
	}
	no::Renderer::Clear();
	testFont.RenderImmediate(0, 0);
	REQUIRE(ApprovalText::Question("Did you see some text in a separate window?") == testTrue);

	no::Renderer::Clear();
	testFont.SetColor({ 1, 1, 0, 1 });
	testFont.RenderImmediate(0, 0);
	REQUIRE(ApprovalText::Question("Did the color become yellow?") == testTrue);

	no::Font testNonExist = no::Font("NoPath", "No Text");
	testNonExist.RenderImmediate(20, 20);
}

void AddComponentsImage()
{
	no::SceneManager::GetInstance().CreateScene("Test");
	auto test = no::GameObject::Create();
	test->AddComponent<no::TransformComponent>();
	auto rc = test->AddComponent<no::RenderComponent>();
	//no::SceneManager::GetInstance().GetScenes()[0]->Add(test);
	rc->SetTexture("Logo.png");
	rc->SetTexture("background.jpg");

	auto test2 = no::GameObject::Create();
	test2->AddComponent<no::TransformComponent>();
	auto textComponent = test2->AddComponent<no::TextComponent>();
	textComponent->Initialize("Lingua.otf", "Hello World!");
	//no::SceneManager::GetInstance().GetScenes()[0]->Add(test2);
}

#ifdef TEST_NO_MODULE

TEST_CASE("Test Image module missing")
{
	ApprovalText::Title("Test Image No Module");

	std::vector<no::Module*> modules{};
	
	no::Core engine{ modules, "../Data/" };
	engine.Initialize();

	// test resources
	RenderImages(false);
	RenderFonts(false);

	AddComponentsImage();

	engine.Step(0.02f);
	REQUIRE_FALSE(ApprovalText::Question("Did you see a separate window?"));
	REQUIRE_FALSE(ApprovalText::Question("Did you see images or text in a separate window?"));
}
#endif

#ifdef TEST_SDL

TEST_CASE("Test SDL Image Module")
{
	ApprovalText::Title("Test Image SDL Module");

	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSDLImage(640, 480, "Test SDL Image module"));
	modules.push_back(new no::ModuleSDLEvent());

	no::Core engine{ modules, "../Data/" };
	engine.Initialize();

	// test resources
	RenderImages(true);
	RenderFonts(true);

	AddComponentsImage();

	engine.Step(0.02f);
	REQUIRE(ApprovalText::Question("Did you see a separate window?"));
	REQUIRE(ApprovalText::Question("Did you see images or text in a separate window?"));
}
#endif // TEST_SDL

#ifdef TEST_SFML

TEST_CASE("Test SFML Image Module")
{
	ApprovalText::Title("Test Image SFML Module");

	std::vector<no::Module*> modules{};
	modules.push_back(new no::ModuleSFMLImage(640, 480, "Test SFML Image module"));
	modules.push_back(new no::ModuleSFMLEvent());

	no::Core engine{ modules, "../Data/" };
	engine.Initialize();

	// test resources
	RenderImages(true);
	RenderFonts(true);

	AddComponentsImage();

	engine.Step(0.02f);
	REQUIRE(ApprovalText::Question("Did you see a separate window?"));
	REQUIRE(ApprovalText::Question("Did you see images or text in a separate window?"));
}
#endif // TEST_SFML
#endif