#include "ApprovalText.h"
#include <iostream>

bool ApprovalText::Question(const std::string& question)
{
	char ans;
	std::cout << question << "(Y/N) >";
	std::cin >> ans;
	return ans == 'Y' || ans == 'y';
}

void ApprovalText::Title(const std::string& title)
{
	std::cout << '\n';
	std::cout << title << '\n';
	std::cout << "-------------------------------------\n";
}
