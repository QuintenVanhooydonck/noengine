#include "catch.hpp"

#include <NoEngine/Core.h>
#include <NoEngine/Scene/GameObject.h>
#include <NoEngine/Scene/SceneManager.h>
#include <NoEngine/Memory/PointerReference.h>

TEST_CASE("Attach GameObject to scene")
{
	auto engine = no::Core();
	auto scene = no::SceneManager::GetInstance().CreateScene("hello");
	no::PointerReference<no::GameObject> testObject = no::GameObject::Create();
	no::PointerReference<no::GameObject> testObject2 = no::GameObject::Create(testObject);

	engine.Step(0.02f);

	REQUIRE(testObject->GetScene() == scene);
	REQUIRE(testObject2->GetScene() == scene);
	REQUIRE(testObject2->GetParent() == testObject);

	testObject->Destroy();
	engine.Step(0.02f);

	REQUIRE_FALSE(testObject);
	REQUIRE_FALSE(testObject2);
}

TEST_CASE("Move around objects")
{
	auto engine = no::Core();
	auto scene = no::SceneManager::GetInstance().CreateScene("hello");
	no::PointerReference<no::GameObject> testObject = no::GameObject::Create();
	no::PointerReference<no::GameObject> testObject2 = no::GameObject::Create(testObject);

	engine.Step(0.02f);

	REQUIRE(testObject->GetScene() == scene);
	REQUIRE(testObject2->GetScene() == scene);
	REQUIRE(testObject2->GetParent() == testObject);

	testObject2->AttachTo();
	engine.Step(0.02f);

	REQUIRE(testObject->GetScene() == scene);
	REQUIRE(testObject2->GetScene() == scene);
	REQUIRE(testObject2->GetParent() == nullptr);

	testObject->AttachTo(testObject2);
	engine.Step(0.02f);

	REQUIRE(testObject->GetScene() == scene);
	REQUIRE(testObject->GetParent() == testObject2);
	REQUIRE(testObject2->GetScene() == scene);
	REQUIRE(testObject2->GetParent() == nullptr);
}
