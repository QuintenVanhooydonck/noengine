#include "ModuleEditorSFML.h"

#include "NoEngine/Core.h"
#include "SFMLImage/ModuleSFMLImage.h"
#include "SFMLImage/Window/WindowSFML.h"
#include "NoEngine/Render/Window.h"
#include "ImGUI/imgui-SFML.h"
#include "ImGUI/imgui.h"
#include "SFML/System/Clock.hpp"
#include <SFML/Graphics.hpp>
#include "SFMLEvent/ModuleSFMLEvent.h"
#include <SFMLEvent/InputManager/InputManagerSFML.h>

noEditor::ModuleEditorSFML::ModuleEditorSFML()
	:m_Clock{ new sf::Clock() }
{
}

noEditor::ModuleEditorSFML::~ModuleEditorSFML()
{
	ImGui::SFML::Shutdown();
	if (m_Clock)
	{
		delete m_Clock;
		m_Clock = nullptr;
	}
}

void noEditor::ModuleEditorSFML::Render()
{
	sf::Event event;
	while (m_Window->pollEvent(event)) {
		ImGui::SFML::ProcessEvent(event);

		if (event.type == sf::Event::Closed) {
			Core::Quit();
		}
	}

	ImGui::SFML::Update(*m_Window, m_Clock->restart());
	m_EditorEntry.Draw();
	ImGui::SFML::Render(*m_Window);

	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
	}
}

void noEditor::ModuleEditorSFML::Initialize()
{
	Core::RequireModule<ModuleSFMLImage>();

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	auto sfmlWindow = static_cast<WindowSFML*>(Window::GetService());
	m_EditorEntry.Initialize();

	m_Window = sfmlWindow->GetWindow();
	ImGui::SFML::Init(*m_Window);

}
