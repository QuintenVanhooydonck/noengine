#pragma once
#include <NoEngine/Modules/Module.h>
#include <Editor/EditorEntryPoint.h>

using namespace no;

namespace sf { 
	class Clock; 
	class RenderWindow;
	class Event;
}

namespace noEditor
{
	class ModuleEditorSFML final : public Module
	{
	public:
		ModuleEditorSFML();
		~ModuleEditorSFML();

		virtual void Render() override;
		virtual void Initialize() override;

	private:
		sf::RenderWindow* m_Window{};
		EditorEntryPoint m_EditorEntry{};
		sf::Clock* m_Clock;

	};
}