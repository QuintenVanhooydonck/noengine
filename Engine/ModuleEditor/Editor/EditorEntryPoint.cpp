#include "EditorEntryPoint.h"
#include "ImGUI/imgui.h"
#include "Window/EditorWindow.h"

noEditor::EditorEntryPoint::~EditorEntryPoint()
{
	for (auto& window : m_Windows)
	{
		delete window;
	}
	m_Windows.clear();
}

void noEditor::EditorEntryPoint::Initialize()
{
	m_EditorInfo.entryPoint = this;

	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

	ImGui::StyleColorsDark();
}

void noEditor::EditorEntryPoint::Draw()
{
	DrawFullscreendock();

	for (auto& window : m_Windows)
	{
		window->RootGUI();
	}

	RemoveDestroyedWindows();
	AddNewWindows();
}

void noEditor::EditorEntryPoint::DrawScene()
{
	for (auto& window : m_Windows)
	{
		window->RootOnScene();
	}
}

void noEditor::EditorEntryPoint::AddWindow(EditorWindow* window)
{
	m_NewWindows.push_back(window);
}

void noEditor::EditorEntryPoint::RemoveWindow(EditorWindow* window)
{
	m_DestroyedWindows.push_back(window);
}

void noEditor::EditorEntryPoint::DrawFullscreendock()
{
	static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

	bool* p_open = nullptr;

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
	ImGuiViewport* viewport = ImGui::GetMainViewport();
	ImGui::SetNextWindowPos(viewport->GetWorkPos());
	ImGui::SetNextWindowSize(viewport->GetWorkSize());
	ImGui::SetNextWindowViewport(viewport->ID);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
	window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

	if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
		window_flags |= ImGuiWindowFlags_NoBackground;

	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::Begin("Dock Space", p_open, window_flags);
	ImGui::PopStyleVar();

	ImGui::PopStyleVar(2);

	// DockSpace
	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
	{
		ImGuiID dockspace_id = ImGui::GetID("DockSpace");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
	}

	m_MenuBar.Draw(m_EditorInfo);

	ImGui::End();
}

void noEditor::EditorEntryPoint::AddNewWindows()
{
	for (auto window : m_NewWindows)
	{
		m_Windows.push_back(window);
		window->RootInitialize(this);
	}
	m_NewWindows.clear();
}

void noEditor::EditorEntryPoint::RemoveDestroyedWindows()
{
	for (auto window : m_DestroyedWindows)
	{
		auto it = std::find(m_Windows.begin(), m_Windows.end(), window);
		if (it != m_Windows.end())
		{
			m_Windows.erase(it);
			delete window;
		}
	}
	m_DestroyedWindows.clear();
}
