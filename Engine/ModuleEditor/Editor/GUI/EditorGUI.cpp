#include "EditorGUI.h"

bool noEditorGUI::TextField(std::string& text, const std::string& name)
{
	return ImGui::InputText(name.c_str(), (char*)text.c_str(), text.capacity() + 1, 0);
}

void noEditorGUI::ToggleButton(const std::string& name, bool& var)
{
	auto buttonColor = ImGui::GetStyleColorVec4(ImGuiCol_Button);
	auto disableColor = ImVec4{ 0.4f, 0.4f, 0.4f, 1 };

	ImGui::PushStyleColor(ImGuiCol_Button, var ? buttonColor : disableColor);
	if (ImGui::Button(name.c_str()))
		var = !var;
	ImGui::PopStyleColor();
}
