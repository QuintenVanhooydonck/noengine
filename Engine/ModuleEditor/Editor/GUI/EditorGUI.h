#pragma once
#include <string>
#include "ImGUI/imgui.h"

namespace noEditorGUI
{
	//Text
	bool TextField(std::string& text, const std::string& name);

	//Bool
	void ToggleButton(const std::string& name, bool& var);
}