#include "SceneWindow.h"
#include "ImGUI/imgui.h"
#include "../EditorEntryPoint.h"
#include <NoEngine/Render/Renderer.h>
#include <NoEngine/Render/RenderTarget.h>
#include "NoEngine/Scene/SceneManager.h"

using namespace no;

noEditor::SceneWindow::~SceneWindow()
{
	if (m_RenderTarget)
	{
		delete m_RenderTarget;
		m_RenderTarget = nullptr;
	}
}

void noEditor::SceneWindow::OnGUI()
{
	RenderScene();
	ImGui::SetCursorPos({ 10, ImGui::GetFrameHeightWithSpacing() });
	ImGui::BeginGroup();
	m_EditorEntry->DrawScene();
	ImGui::EndGroup();
}

void noEditor::SceneWindow::PushStyleVars() const
{
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, { 1, 1 });
}

void noEditor::SceneWindow::PopStyleVars() const
{
	ImGui::PopStyleVar();
}

ImGuiWindowFlags noEditor::SceneWindow::GetFlags() const
{
	return EditorWindow::GetFlags() | ImGuiWindowFlags_NoScrollbar;
}

void noEditor::SceneWindow::Initialize()
{
	m_RenderTarget = RenderTarget::Create({ 0, 0 });
}

void noEditor::SceneWindow::RenderScene()
{
	auto size = ImGui::GetWindowSize();
	size.y -= ImGui::GetFrameHeightWithSpacing();

	m_RenderTarget->SetTextureSize({ size.x, size.y });
	Renderer::GetService()->SetRenderTarget(m_RenderTarget);
	SceneManager::GetInstance().Render();
	Renderer::GetService()->resetRenderTarget();

	bool flipVert{}, flipHor{};
	void* imagePointer = m_RenderTarget->GetImagePointer(&flipHor, &flipVert);
	ImVec2 uv0{ 0.f, 0.f }, uv1{ 1.f, 1.f };
	if (flipHor)
	{
		uv0.x = 1;
		uv1.x = 0;
	}
	if (flipVert)
	{
		uv0.y = 1;
		uv1.y = 0;
	}

	ImGui::Image(imagePointer, size, uv0, uv1);
}

MENU_ITEM_ORDERED(Scene, "Window/General", -500, &noEditor::SceneWindow::CreateSceneWindow);
void noEditor::SceneWindow::CreateSceneWindow(const EditorInfo& editorInfo)
{
	editorInfo.entryPoint->AddNewWindow<SceneWindow>();
}
