#include "EditorWindow.h"
#include <typeinfo>
#include "ImGUI/imgui.h"
#include <regex>
#include "../EditorEntryPoint.h"

noEditor::EditorWindow::EditorWindow(int id)
	:m_Id{ id }
{
}

void noEditor::EditorWindow::RootInitialize(EditorEntryPoint* editorEntry)
{
	m_EditorEntry = editorEntry;
	m_Name = GetName() + "##" + std::to_string(m_Id);
	Initialize();
}

void noEditor::EditorWindow::RootOnScene()
{
	OnScene();
}

std::string noEditor::EditorWindow::GetName() const
{
	std::string windowName = std::string(typeid(*this).name());

	std::regex classReg{ R"~((?:.* )(?:.*::)?(\w*))~" };
	std::smatch matches;
	std::regex_search(windowName, matches, classReg);
	windowName = matches[1];

	std::vector<std::string> filters
	{
		"[Ee]ditor",
		"[Ww]indow"
	};

	for (auto& filter : filters)
	{
		std::regex filterReg{"(\\w*)(?:" + filter + ")"};
		std::regex_search(windowName, matches, filterReg);
		if (!matches.empty())
			windowName = matches[1];
	}

	return std::string(matches.str().c_str());
}

void noEditor::EditorWindow::RootGUI()
{
	PushStyleVars();

	ImGui::Begin(m_Name.c_str(), &m_Open, GetFlags());
	OnGUI();
	ImGui::End();
	if (!m_Open)
	{
		m_EditorEntry->RemoveWindow(this);
	}

	PopStyleVars();
}
