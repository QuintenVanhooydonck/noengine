#pragma once
#include <string>
#include "ImGUI/imgui.h"
#include "Editor/Menus/MenuBar.h"
#include "Editor/EditorInfo.h"

namespace noEditor
{
	class EditorEntryPoint;

	class EditorWindow
	{
		friend class SceneWindow;

	public:
		EditorWindow(int id);
		virtual ~EditorWindow() = default;

		EditorWindow(const EditorWindow&) = delete;
		EditorWindow(EditorWindow&&) = delete;
		EditorWindow& operator=(const EditorWindow&) = delete;
		EditorWindow& operator=(EditorWindow&&) = delete;

		void RootGUI();
		void RootInitialize(EditorEntryPoint* editorEntry);
		void RootOnScene();

		int GetID() const { return m_Id; }

	protected:
		virtual void OnGUI() = 0;
		virtual void OnScene() {};
		virtual void Initialize() {}
		virtual std::string GetName() const;
		
		virtual void PushStyleVars() const {}
		virtual void PopStyleVars() const {}
		virtual ImGuiWindowFlags GetFlags() const { return 0; }

	private:
		std::string m_Name{};
		EditorEntryPoint* m_EditorEntry{};
		int m_Id;
		bool m_Open{ true };
	};
}