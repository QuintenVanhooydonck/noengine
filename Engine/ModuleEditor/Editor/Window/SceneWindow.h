#pragma once
#include "EditorWindow.h"
#include <NoEngine/Debug/Logger.h>

namespace no
{
	class RenderTarget;
}

namespace noEditor
{
	class SceneWindow : public EditorWindow
	{
	public:
		static void CreateSceneWindow(const EditorInfo& editorInfo);

		SceneWindow(int id) : EditorWindow(id) {};
		~SceneWindow();

	protected:
		virtual void OnGUI() override;

		virtual void PushStyleVars() const override;
		virtual void PopStyleVars() const override;
		virtual ImGuiWindowFlags GetFlags() const override;

		virtual void Initialize() override;

	private:
		no::RenderTarget* m_RenderTarget{};

		void RenderScene();
	};
}