#pragma once
#include "EditorWindow.h"
#include <NoEngine/Debug/Internal/ILogger.h>
#include <NoEngine/Debug/Logger.h>
#include <vector>

using namespace no;

namespace noEditor
{
	class LoggingWindow : public EditorWindow, ILogger
	{
	private:
		struct LogData
		{
			no::LogType type;
			std::string logText;
			std::string file;
			std::string functionLine;
		};

	public:
		static void CreateLoggingWindow(const EditorInfo& editorInfo);
		LoggingWindow(int id) : EditorWindow(id) {}
		~LoggingWindow();
		bool m_IsDeleted = false;

		virtual void Log(const std::string& info, const std::string& file, const std::string& function, int line) override;
		virtual void Warning(const std::string& warning, const std::string& file, const std::string& function, int line) override;
		virtual void Exception(const std::string& error, const std::string& file, const std::string& function, int line) override;

	protected:
		virtual void OnGUI() override;
		virtual void Initialize() override;

	private:
		const size_t m_MaxLogs{100};
		const ImVec4 m_ColorLog{ 1, 1, 1, 1 }, m_ColorWarning{ 1, 1, 0.3f, 1 }, m_ColorException{1, 0.3f, 0.3f, 1};
		std::vector<LogData> m_LogData{};
		std::string m_SearchText{};
		size_t m_LogID{};
		bool m_EnableInfo{ true }, m_EnableWarning{ true }, m_EnableError{ true };

		void SaveLog(no::LogType logType, const std::string& logText, const std::string& file, const std::string& function, int line);
		bool LogToWindow(const LogData& logData);
		bool MatchSearch(const LogData& logData);
	};
}