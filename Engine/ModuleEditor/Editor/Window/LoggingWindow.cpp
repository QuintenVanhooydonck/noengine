#include "LoggingWindow.h"
#include "Editor/EditorEntryPoint.h"
#include "Editor/GUI/EditorGUI.h"
#include "ImGUI/imgui_stdlib.h"

MENU_ITEM_ORDERED(Logging, "Window/General", -400, &noEditor::LoggingWindow::CreateLoggingWindow)
void noEditor::LoggingWindow::CreateLoggingWindow(const EditorInfo& editorInfo)
{
	editorInfo.entryPoint->AddNewWindow<LoggingWindow>();
}

noEditor::LoggingWindow::~LoggingWindow()
{
	Logger::Remove(m_LogID);
}

void noEditor::LoggingWindow::Log(const std::string& info, const std::string& file, const std::string& function, int line)
{
	SaveLog(LogType::Info, info, file, function, line);
}

void noEditor::LoggingWindow::Warning(const std::string& warning, const std::string& file, const std::string& function, int line)
{
	SaveLog(LogType::Warning, warning, file, function, line);
}

void noEditor::LoggingWindow::Exception(const std::string& error, const std::string& file, const std::string& function, int line)
{
	SaveLog(LogType::Error, error, file, function, line);
}

void noEditor::LoggingWindow::OnGUI()
{
	if (ImGui::Button("Clear"))
	{
		m_LogData.clear();
	}
	ImGui::SameLine();
	noEditorGUI::ToggleButton("Info", m_EnableInfo);
	ImGui::SameLine();
	noEditorGUI::ToggleButton("Warning", m_EnableWarning);
	ImGui::SameLine();
	noEditorGUI::ToggleButton("Error", m_EnableError);
	ImGui::SameLine();
	ImGui::InputText("##Search", &m_SearchText);

	size_t numLogs{};
	for (size_t i = m_LogData.size(); i > 0; --i)
	{
		if (LogToWindow(m_LogData[i - 1]))
		{
			++numLogs;
			if(numLogs >= m_MaxLogs)
				break;
		}
	}
}

void noEditor::LoggingWindow::Initialize()
{
	m_LogID = Logger::Provide(this);
}

void noEditor::LoggingWindow::SaveLog(no::LogType logType, const std::string& logText, const std::string& file, const std::string& function, int line)
{
	LogData logData{};
	logData.type = logType;
	logData.logText = logText;
	logData.file = file;
	logData.functionLine = function + ":" + std::to_string(line);
	m_LogData.push_back(logData);
}

bool noEditor::LoggingWindow::LogToWindow(const LogData& logData)
{
	if (!m_SearchText.empty())
	{
		if (!MatchSearch(logData))
			return false;
	}

	bool isSelected = false;


	switch (logData.type)
	{
	case LogType::Warning:
		if (!m_EnableWarning) return false;
		ImGui::PushStyleColor(ImGuiCol_Text, m_ColorWarning);
		break;
	case LogType::Error:
		if (!m_EnableError) return false;
		ImGui::PushStyleColor(ImGuiCol_Text, m_ColorException);
		break;
	case LogType::Info:
	default:
		if (!m_EnableInfo) return false;
		ImGui::PushStyleColor(ImGuiCol_Text, m_ColorLog);
		break;
	}

	ImGui::PushStyleColor(ImGuiCol_Button, { 0, 0, 0, 0 });
	ImGui::PushItemWidth(ImGui::GetColumnWidth());
	auto cursorPos = ImGui::GetCursorPos();
	cursorPos.y += ImGui::GetStyle().FramePadding.y;

	isSelected = ImGui::Button("", { -1, 0 });
	ImGui::SetCursorPos(cursorPos);
	ImGui::Text(logData.logText.c_str());

	ImGui::PopItemWidth();
	ImGui::PopStyleColor(2);

	return true;
}

bool noEditor::LoggingWindow::MatchSearch(const LogData& logData)
{
	return logData.logText.find(m_SearchText) != std::string::npos ||
		logData.file.find(m_SearchText) != std::string::npos ||
		logData.functionLine.find(m_SearchText) != std::string::npos;
}
