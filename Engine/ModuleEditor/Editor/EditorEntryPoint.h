#pragma once
#include <vector>
#include <typeinfo>
#include "Menus/MenuBar.h"
#include "EditorInfo.h"

namespace noEditor
{
	class EditorWindow;

	class EditorEntryPoint final
	{
	public:
		EditorEntryPoint() = default;
		~EditorEntryPoint();
		
		EditorEntryPoint(const EditorEntryPoint&) = delete;
		EditorEntryPoint(EditorEntryPoint&&) = delete;
		EditorEntryPoint& operator=(const EditorEntryPoint&) = delete;
		EditorEntryPoint& operator=(EditorEntryPoint&&) = delete;

		void Initialize();
		void Draw();
		void DrawScene();

		// only adds a window if no other exists
		template <typename T>
		void AddWindow();
		// adds a new window if window with id doesn't exist yet
		template <typename T>
		void AddWindow(int id);
		// adds the new window that is created by the user
		void AddWindow(EditorWindow* window);
		// adds a new window with an unused id
		template <typename T>
		void AddNewWindow();
		void RemoveWindow(EditorWindow* window);

	private:
		std::vector<EditorWindow*> m_Windows{};
		std::vector<EditorWindow*> m_NewWindows{};
		std::vector<EditorWindow*> m_DestroyedWindows{};
		MenuBar m_MenuBar{false};
		EditorInfo m_EditorInfo{};

		void DrawFullscreendock();
		void AddNewWindows();
		void RemoveDestroyedWindows();
	};
	
	template<typename T>
	inline void EditorEntryPoint::AddWindow()
	{
		const type_info& info = typeid(T);
		for (auto window : m_Windows)
		{
			if (window && typeid(*window) == info)
				return;
		}

		AddWindow(new T(0));
	}

	template<typename T>
	inline void EditorEntryPoint::AddWindow(int id)
	{
		const type_info& info = typeid(T);

		for (auto window : m_Windows)
		{
			if (window && typeid(*window) == info &&
				window->GetID() == id)
				return;
		}

		AddWindow(new T(id));
	}

	template<typename T>
	inline void EditorEntryPoint::AddNewWindow()
	{
		int id = 0;
		bool idFound = true;

		const type_info& info = typeid(T);
		while (idFound)
		{
			idFound = false;
			for (auto window : m_Windows)
			{
				if (window && typeid(*window) == info &&
					window->GetID() == id)
				{
					idFound = true;
					++id;
					break;
				}
			}
		}

		AddWindow(new T(id));
	}
}