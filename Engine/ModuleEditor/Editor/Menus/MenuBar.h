#pragma once
#include <set>
#include <string>


namespace noEditor
{
	struct EditorInfo;
	struct MenuItemData;

	typedef void(*itemFunction)(const EditorInfo& editorInfo);

	class MenuBar final
	{
	private:
		struct MenuItemData
		{
			std::string path;
			itemFunction function;
			int order;

			bool operator<(const MenuItemData& rhs) const
			{
				return order < rhs.order;
			}
		};

	public:
		MenuBar(bool isMainMenu);
		void Draw(const EditorInfo& editorInfo);

		static void RegisterMenuItem(const std::string& itemName, itemFunction function, int order = 0);

	private:
		static std::set<MenuItemData> m_MenuItems;
		const bool m_IsMainMenu;

		bool BeginMenu();
		void EndMenu();
		bool DrawMenuItem(const std::string& itemName);
	};

	class MenuBarRegistration final
	{
	public:
		MenuBarRegistration(const std::string& itemName, itemFunction function, int order = 0)
		{
			MenuBar::RegisterMenuItem(itemName, function, order);
		}
	};

#define MENU_ITEM(name, menupath, function) noEditor::MenuBarRegistration _##name(menupath + std::string("/") + #name, function);
#define MENU_ITEM_ORDERED(name, menupath, order, function) noEditor::MenuBarRegistration _##name(menupath + std::string("/") + #name, function, order);
}