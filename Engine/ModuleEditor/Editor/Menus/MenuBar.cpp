#include "MenuBar.h"
#include "ImGUI/imgui.h"
#include "../EditorInfo.h"
#include <sstream>
#include <vector>

std::set<noEditor::MenuBar::MenuItemData> noEditor::MenuBar::m_MenuItems{};

noEditor::MenuBar::MenuBar(bool isMainMenu)
	:m_IsMainMenu(isMainMenu)
{
}

void noEditor::MenuBar::Draw(const EditorInfo& editorInfo)
{
	if (BeginMenu())
	{
		for (const auto& menuItem : m_MenuItems)
		{
			if (DrawMenuItem(menuItem.path))
			{
				menuItem.function(editorInfo);
			}
		}
		EndMenu();
	}
}

void noEditor::MenuBar::RegisterMenuItem(const std::string& itemName, itemFunction function, int order)
{
	m_MenuItems.insert({ itemName, function, order});
}

bool noEditor::MenuBar::BeginMenu()
{
	if (m_IsMainMenu)
		return ImGui::BeginMainMenuBar();
	else
		return ImGui::BeginMenuBar();
}

void noEditor::MenuBar::EndMenu()
{
	if (m_IsMainMenu)
		ImGui::EndMainMenuBar();
	else
		ImGui::EndMenuBar();
}

bool noEditor::MenuBar::DrawMenuItem(const std::string& itemName)
{
	bool isMenuClicked = false, menuItemVisible = true;
	size_t menusOpen = 0;

	// tokenize menu layout
	std::stringstream itemStream(itemName);
	std::vector<std::string> tokens{};
	std::string intermediate;
	while (std::getline(itemStream, intermediate, '/'))
	{
		tokens.push_back(intermediate);
	}

	// open menus
	for (size_t i = 0; i < tokens.size() - 1; i++)
	{
		if (ImGui::BeginMenu(tokens[i].c_str()))
			++menusOpen;
		else
		{
			menuItemVisible = false;
			break;
		}
	}

	// show menu item
	if(menuItemVisible)
		isMenuClicked = ImGui::MenuItem(tokens[tokens.size() - 1].c_str());

	// close menus
	for (size_t i = 0; i < menusOpen; i++)
	{
		ImGui::EndMenu();
	}

	return isMenuClicked;
}
