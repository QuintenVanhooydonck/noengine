#pragma once

namespace noEditor
{
	class EditorEntryPoint;

	struct EditorInfo
	{
		EditorEntryPoint* entryPoint;
	};
}