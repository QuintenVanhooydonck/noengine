#pragma once
#include <NoEngine/Modules/Module.h>
#include <Editor/EditorEntryPoint.h>

using namespace no;

struct SDL_Window;

namespace noEditor
{
	class ModuleEditorSDL final : public Module
	{
	public:
		ModuleEditorSDL();
		~ModuleEditorSDL();

		virtual void Render() override;
		virtual void Initialize() override;

	private:
		SDL_Window* m_Window{};
		EditorEntryPoint m_EditorEntry{};
	};
}