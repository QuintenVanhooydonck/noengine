#include "ModuleEditorSDL.h"

#include <SDL.h>
#pragma warning(push)
#pragma warning(disable:4121)
#include <SDL_syswm.h>
#pragma warning(pop)

#include <NoEngine/Core.h>
#include <NoEngine/Render/Window.h>

#include <ImGUI/imgui.h>
#include <SDLImage/ModuleSDLImage.h>
#include <SDLImage/Window/WindowSDL.h>
#include "ImGUI/imgui_impl_sdl.h"
#include "ImGUI/imgui_impl_opengl2.h"

noEditor::ModuleEditorSDL::ModuleEditorSDL()
{
}

noEditor::ModuleEditorSDL::~ModuleEditorSDL()
{
	ImGui_ImplOpenGL2_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();
}

void noEditor::ModuleEditorSDL::Render()
{
	ImGui_ImplOpenGL2_NewFrame();
	ImGui_ImplSDL2_NewFrame(m_Window);
	ImGui::NewFrame();

	m_EditorEntry.Draw();

	ImGui::Render();
	ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());

	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		SDL_GLContext backup_current_context = SDL_GL_GetCurrentContext();
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		SDL_GL_MakeCurrent(m_Window, backup_current_context);
	}
}

void noEditor::ModuleEditorSDL::Initialize()
{
	Core::RequireModule<ModuleSDLImage>();

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	m_EditorEntry.Initialize();

	auto sdlWindow = static_cast<WindowSDL*>(Window::GetService());
	m_Window = sdlWindow->GetWindowHandle();

	SDL_SysWMinfo systemInfo;
	SDL_VERSION(&systemInfo.version);
	SDL_GetWindowWMInfo(m_Window, &systemInfo);
	ImGui_ImplOpenGL2_Init();
	ImGui_ImplSDL2_InitForOpenGL(m_Window, SDL_GL_GetCurrentContext());

}
